/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 5.6.12-log : Database - enrollment_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`enrollment_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `enrollment_db`;

/*Table structure for table `tbl_billings` */

DROP TABLE IF EXISTS `tbl_billings`;

CREATE TABLE `tbl_billings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_id` int(11) DEFAULT NULL,
  `tuition_fee` double DEFAULT '0',
  `reg_fee` double DEFAULT '0',
  `misc_fee` double DEFAULT '0',
  `computer` double DEFAULT '0',
  `aircon` double DEFAULT '0',
  `books` double DEFAULT '0',
  `total` double DEFAULT '0',
  `discount_cash` double DEFAULT '0',
  `discount_2ndchild` double DEFAULT '0',
  `discount_3rdchild` double DEFAULT '0',
  `discount_4thchild` double DEFAULT '0',
  `discount_oldstudent` double DEFAULT '0',
  `discount_gis` double DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_billings` */

insert  into `tbl_billings`(`id`,`grade_id`,`tuition_fee`,`reg_fee`,`misc_fee`,`computer`,`aircon`,`books`,`total`,`discount_cash`,`discount_2ndchild`,`discount_3rdchild`,`discount_4thchild`,`discount_oldstudent`,`discount_gis`) values 
(1,1,500,1000,1000,1000,1000,1000,5500,1,1,1,1,1,1),
(2,2,1500,1500,1500,1500,1500,1500,9000,0,0,0,0,0,0),
(3,3,2000,2000,2000,2000,2000,2000,12000,0,0,0,0,0,0),
(4,4,2500,2500,2500,2500,2500,2500,15000,0,0,0,0,0,0),
(5,5,3000,3000,3000,3000,3000,3000,18000,0,0,0,0,0,0),
(6,6,3500,3500,3500,3500,3500,3500,21000,0,0,0,0,0,0),
(7,7,4000,4000,4000,4000,4000,4000,24000,0,0,0,0,0,0),
(8,8,4500,4500,4500,4500,4500,4500,27000,10,10,10,10,10,10),
(9,9,5000,500,1500,2500,600,2000,12100,0,0,0,0,0,0),
(10,10,0,0,0,0,0,0,0,0,0,0,0,0,0),
(11,11,0,0,0,0,0,0,0,0,0,0,0,0,0),
(12,12,0,0,0,0,0,0,0,0,0,0,0,0,0),
(13,13,0,0,0,0,0,0,0,0,0,0,0,0,0),
(14,14,0,0,0,0,0,0,0,0,0,0,0,0,0),
(15,15,0,0,0,0,0,0,0,0,0,0,0,0,0),
(16,1,500,1000,1000,1000,1000,1000,5500,0,0,0,0,0,0),
(17,2,1,0,0,0,0,0,0,0,0,0,0,0,0),
(18,3,1,0,0,0,0,0,0,0,0,0,0,0,0),
(19,4,1,0,0,0,0,0,0,0,0,0,0,0,0),
(20,5,1,0,0,0,0,0,0,0,0,0,0,0,0),
(21,6,1,0,0,0,0,0,0,0,0,0,0,0,0),
(22,7,1,0,0,0,0,0,0,0,0,0,0,0,0),
(23,8,1,0,0,0,0,0,0,0,0,0,0,0,0),
(24,9,1,0,0,0,0,0,0,0,0,0,0,0,0),
(25,10,1,0,0,0,0,0,0,0,0,0,0,0,0),
(26,11,1,0,0,0,0,0,0,0,0,0,0,0,0),
(27,12,1,0,0,0,0,0,0,0,0,0,0,0,0),
(28,13,1,0,0,0,0,0,0,0,0,0,0,0,0),
(29,14,1,0,0,0,0,0,0,0,0,0,0,0,0),
(30,15,1,0,0,0,0,0,0,0,0,0,0,0,0);

/*Table structure for table `tbl_enrollment_form` */

DROP TABLE IF EXISTS `tbl_enrollment_form`;

CREATE TABLE `tbl_enrollment_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `stud_address` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `grade_level` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `f_religion` varchar(255) DEFAULT NULL,
  `f_nationality` varchar(255) DEFAULT NULL,
  `f_educ_attain` varchar(255) DEFAULT NULL,
  `f_occupation` varchar(255) DEFAULT NULL,
  `f_contact` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `m_religion` varchar(255) DEFAULT NULL,
  `m_nationality` varchar(255) DEFAULT NULL,
  `m_educ_attain` varchar(255) DEFAULT NULL,
  `m_occupation` varchar(255) DEFAULT NULL,
  `m_contact` varchar(255) DEFAULT NULL,
  `lives_with` varchar(255) DEFAULT NULL,
  `lives_with_other` varchar(255) DEFAULT NULL,
  `recommend_by` varchar(255) DEFAULT NULL,
  `prev_school` varchar(255) DEFAULT NULL,
  `prev_school_city` varchar(255) DEFAULT NULL,
  `sibling1_name` varchar(255) DEFAULT NULL,
  `sibling1_grade` varchar(255) DEFAULT NULL,
  `sibling2_name` varchar(255) DEFAULT NULL,
  `sibling2_grade` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `g_occupation` varchar(255) DEFAULT NULL,
  `g_contact` varchar(255) DEFAULT NULL,
  `g_relation` varchar(255) DEFAULT NULL,
  `g_church_affil` varchar(255) DEFAULT NULL,
  `g_company` varchar(255) DEFAULT NULL,
  `g_phoneno` varchar(255) DEFAULT NULL,
  `g_email` varchar(255) DEFAULT NULL,
  `emergency_name` varchar(255) DEFAULT NULL,
  `e_relation` varchar(255) DEFAULT NULL,
  `e_contact` varchar(255) DEFAULT NULL,
  `e_address` varchar(255) DEFAULT NULL,
  `sickness` varchar(255) DEFAULT NULL,
  `date_save` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Not Enrolled',
  `section_id` int(11) DEFAULT '0',
  `school_year` varchar(255) DEFAULT NULL,
  `stud_type` varchar(255) DEFAULT NULL,
  `enroll_id` varchar(255) DEFAULT NULL,
  `date_enrolled` datetime DEFAULT NULL,
  `old_student` enum('NEW','OLD') DEFAULT 'NEW',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_enrollment_form` */

/*Table structure for table `tbl_grade_level` */

DROP TABLE IF EXISTS `tbl_grade_level`;

CREATE TABLE `tbl_grade_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_grade_level` */

insert  into `tbl_grade_level`(`id`,`grade_level`) values 
(1,'PREPARATORY'),
(2,'NURSERY'),
(3,'KINDERGARTEN'),
(4,'GRADE I'),
(5,'GRADE II'),
(6,'GRADE III'),
(7,'GRADE IV'),
(8,'GRADE V'),
(9,'GRADE VI'),
(10,'GRADE VII'),
(11,'GRADE VIII'),
(12,'GRADE IX'),
(13,'GRADE X'),
(14,'GRADE XI'),
(15,'GRADE XII');

/*Table structure for table `tbl_log_in` */

DROP TABLE IF EXISTS `tbl_log_in`;

CREATE TABLE `tbl_log_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_level` varchar(255) DEFAULT NULL,
  `last_log` datetime DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_log_in` */

insert  into `tbl_log_in`(`id`,`username`,`password`,`user_level`,`last_log`,`status`) values 
(2,'cashier','12345','cashier','2018-12-10 07:27:25',1),
(3,'registrar','12345','registrar','2018-12-10 07:28:01',1);

/*Table structure for table `tbl_school_year` */

DROP TABLE IF EXISTS `tbl_school_year`;

CREATE TABLE `tbl_school_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_year` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'INACTIVE',
  `has_data` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_school_year` */

insert  into `tbl_school_year`(`id`,`school_year`,`status`,`has_data`) values 
(1,'2018-2019','ACTIVE',1);

/*Table structure for table `tbl_section` */

DROP TABLE IF EXISTS `tbl_section`;

CREATE TABLE `tbl_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(255) DEFAULT NULL,
  `type` int(1) DEFAULT NULL,
  `grade` int(1) DEFAULT NULL,
  `school_year` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_section` */

insert  into `tbl_section`(`id`,`section`,`type`,`grade`,`school_year`) values 
(0,'none',0,0,1),
(1,'Jodie',1,1,1),
(2,'Jodie',1,2,1),
(3,'Jodie',1,3,1),
(4,'Bettina',1,4,1),
(5,'Annika',1,5,1),
(6,'Margarete',1,6,1),
(7,'Melody',1,7,1),
(9,'John Paul',1,8,1),
(10,'Timothy Paul',1,9,1);

/*Table structure for table `tbl_section_content` */

DROP TABLE IF EXISTS `tbl_section_content`;

CREATE TABLE `tbl_section_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `subjects` int(11) DEFAULT NULL,
  `time` varchar(255) DEFAULT '08:00-09:00',
  `day` varchar(255) DEFAULT 'MTWThF',
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_section_content` */

insert  into `tbl_section_content`(`id`,`section_id`,`subjects`,`time`,`day`,`teacher_id`) values 
(1,1,1,'01:00-02:00','MTWThF',NULL),
(2,1,2,'01:00-02:00','MTWThF',NULL),
(3,1,3,'01:00-02:00','MTWThF',NULL),
(4,1,4,'01:00-02:00','MTWThF',NULL),
(5,1,5,'01:00-02:00','MTWThF',NULL),
(6,1,6,'01:00-02:00','MTWThF',NULL),
(7,1,7,'01:00-02:00','MTWThF',NULL),
(8,1,8,'01:00-02:00','MTWThF',NULL),
(9,2,8,'01:00-02:00','MTWThF',NULL),
(10,2,7,'01:00-02:00','MTWThF',NULL),
(11,2,6,'01:00-02:00','MTWThF',NULL),
(12,2,5,'01:00-02:00','MTWThF',NULL),
(13,2,4,'01:00-02:00','MTWThF',NULL),
(14,2,3,'01:00-02:00','MTWThF',NULL),
(15,2,2,'01:00-02:00','MTWThF',NULL),
(16,2,1,'01:00-02:00','MTWThF',NULL),
(17,3,1,'07:30-08:30','MTWThF',NULL),
(18,3,2,'08:30-09:00','MTWThF',NULL),
(19,3,3,'09:30-10:00','MTWThF',NULL),
(20,3,4,'10:00-10:30','MTWThF',NULL),
(21,3,5,'01:00-01:30','MTWThF',NULL),
(22,3,6,'01:30-02:00','MTWThF',NULL),
(23,3,7,'02:30-03:00','MTWThF',NULL),
(24,3,8,'03:00-03:30','MTWThF',NULL),
(25,4,1,'07:30-08:30','MTWThF',NULL),
(26,4,2,'08:30-09:00','MTWThF',NULL),
(27,4,3,'09:30-10:00','MTWThF',NULL),
(28,4,4,'10:00-10:30','MTWThF',NULL),
(29,4,5,'01:00-01:30','MTWThF',NULL),
(30,4,6,'01:30-02:00','MTWThF',NULL),
(31,4,7,'02:30-03:00','MTWThF',NULL),
(32,4,8,'03:00-03:30','MTWThF',NULL),
(33,5,1,'07:30-08:30','MTWThF',NULL),
(34,5,2,'08:30-09:00','MTWThF',NULL),
(35,5,3,'09:30-10:00','MTWThF',NULL),
(36,5,4,'10:00-10:30','MTWThF',NULL),
(37,5,5,'01:00-01:30','MTWThF',NULL),
(38,5,6,'01:30-02:00','MTWThF',NULL),
(39,5,7,'02:30-03:00','MTWThF',NULL),
(40,5,8,'03:00-03:30','MTWThF',NULL),
(41,6,1,'07:30-08:30','MTWThF',NULL),
(42,6,2,'08:30-09:00','MTWThF',NULL),
(43,6,3,'09:30-10:00','MTWThF',NULL),
(44,6,4,'10:00-10:30','MTWThF',NULL),
(45,6,5,'01:00-01:30','MTWThF',NULL),
(46,6,6,'01:30-02:00','MTWThF',NULL),
(47,6,7,'02:30-03:00','MTWThF',NULL),
(48,6,8,'03:00-03:30','MTWThF',NULL),
(49,7,1,'07:30-08:30','MTWThF',NULL),
(50,7,2,'08:30-09:00','MTWThF',NULL),
(51,7,3,'09:30-10:00','MTWThF',NULL),
(52,7,4,'10:00-10:30','MTWThF',NULL),
(53,7,5,'01:00-01:30','MTWThF',NULL),
(54,7,6,'01:30-02:00','MTWThF',NULL),
(55,7,7,'02:30-03:00','MTWThF',NULL),
(56,7,8,'03:00-03:30','MTWThF',NULL),
(57,8,1,'07:30-08:30','MTWThF',NULL),
(58,8,2,'08:30-09:00','MTWThF',NULL),
(59,8,3,'09:30-10:00','MTWThF',NULL),
(60,8,4,'10:00-10:30','MTWThF',NULL),
(61,8,5,'01:00-01:30','MTWThF',NULL),
(62,8,6,'01:30-02:00','MTWThF',NULL),
(63,8,7,'02:30-03:00','MTWThF',NULL),
(64,8,8,'03:00-03:30','MTWThF',NULL),
(65,9,1,'08:00-09:00','MTWThF',NULL),
(66,9,2,'08:00-09:00','MTWThF',NULL),
(67,9,3,'08:00-09:00','MTWThF',NULL),
(68,9,4,'08:00-09:00','MTWThF',NULL),
(69,9,5,'08:00-09:00','MTWThF',NULL),
(70,9,6,'08:00-09:00','MTWThF',NULL),
(71,9,7,'08:00-09:00','MTWThF',NULL),
(72,9,8,'08:00-09:00','MTWThF',NULL);

/*Table structure for table `tbl_sickness` */

DROP TABLE IF EXISTS `tbl_sickness`;

CREATE TABLE `tbl_sickness` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sick_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_sickness` */

insert  into `tbl_sickness`(`id`,`sick_name`) values 
(1,'Asthma'),
(2,'Allergy'),
(3,'Weak Lungs'),
(4,'Heart Illness');

/*Table structure for table `tbl_student_ledger` */

DROP TABLE IF EXISTS `tbl_student_ledger`;

CREATE TABLE `tbl_student_ledger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'Tuition',
  `payment` double(11,2) DEFAULT '0.00',
  `balance` double(11,2) DEFAULT '0.00',
  `date_trans` datetime DEFAULT NULL,
  `Or_number` varchar(255) DEFAULT NULL,
  `school_year` varchar(255) DEFAULT NULL,
  `student_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_student_ledger` */

insert  into `tbl_student_ledger`(`id`,`stud_id`,`type`,`payment`,`balance`,`date_trans`,`Or_number`,`school_year`,`student_id`) values 
(1,'2','Tuition',0.00,5500.00,'2018-12-10 07:23:58',NULL,'2018-2019','Student-182'),
(2,'3','Tuition',0.00,5500.00,'2018-12-10 07:25:49',NULL,'2018-2019','Student-168'),
(3,'3','Full Payment',5500.00,0.00,'2018-12-10 07:26:08','#20181210646','2018-2019','Student-168'),
(4,'4','Tuition',0.00,5500.00,'2018-12-10 07:27:17',NULL,'2018-2019','Student-183'),
(5,'4','Partial Payment',100.00,5400.00,'2018-12-10 07:27:41','#20181210513','2018-2019','Student-183'),
(6,'4','Full Payment',5400.00,0.00,'2018-12-10 07:27:52','#20181210988','2018-2019','Student-183');

/*Table structure for table `tbl_subjects` */

DROP TABLE IF EXISTS `tbl_subjects`;

CREATE TABLE `tbl_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_code` varchar(11) DEFAULT NULL,
  `subject_name` varchar(255) DEFAULT NULL,
  `stud_type` varchar(255) DEFAULT 'ELEMENTARY',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_subjects` */

insert  into `tbl_subjects`(`id`,`subject_code`,`subject_name`,`stud_type`) values 
(1,'Val','Values(ESP)','ELEMENTARY'),
(2,'Eng','English','ELEMENTARY'),
(3,'MATH','Mathematics','ELEMENTARY'),
(4,'COMPUTER','Computer Basic','ELEMENTARY'),
(5,'MAPEH','Mapeh','ELEMENTARY'),
(6,'FILIPINO','Filipino','ELEMENTARY'),
(7,'HEKASI','Hekasi','ELEMENTARY'),
(8,'EPP','EPP','ELEMENTARY');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
