<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['f_search']);
$sy = mysqli_real_escape_string($con, $_POST['f_sy']);
$grade = mysqli_real_escape_string($con, $_POST['f_grade']);
$section = mysqli_real_escape_string($con, $_POST['f_section']);

if ($search == "" || $search == null) {

	if($sy == 0 and $grade == 0 and $section == ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled';");
	}elseif($sy != 0 and $grade == '' and $section == ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy';");
	}
	elseif($sy != 0 and $grade != '' and $section == ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy' and t1.`grade_level` = '$grade';");
	}
	elseif($sy != 0 and $grade != '' and $section != ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy' and t1.`grade_level` = '$grade' and t1.`section_id` = '$section';");
	}else{
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t3.`id` as sy,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled';");
	}
	
	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['stud_id']; ?></td>
				<td><?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']; ?></td>
				<td><?php echo $row['grade_level']?></td>
				<td><?php echo $row['section']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<div class="btn-group btn-justified text-center"> 
						<button class="btn btn-sm btn-default" onclick="update_student('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['birth_date'] ?>','<?php echo $row['age'] ?>','<?php echo $row['stud_address'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['grade_level'] ?>','<?php echo $row['language'] ?>','<?php echo $row['city'] ?>','<?php echo $row['father_name'] ?>','<?php echo $row['f_religion'] ?>','<?php echo $row['f_nationality'] ?>','<?php echo $row['f_educ_attain'] ?>','<?php echo $row['f_occupation'] ?>','<?php echo $row['f_contact'] ?>','<?php echo $row['mother_name'] ?>','<?php echo $row['m_religion'] ?>','<?php echo $row['m_nationality'] ?>','<?php echo $row['m_educ_attain'] ?>','<?php echo $row['m_occupation'] ?>','<?php echo $row['m_contact'] ?>','<?php echo $row['lives_with'] ?>','<?php echo $row['lives_with_other'] ?>','<?php echo $row['recommend_by'] ?>','<?php echo $row['prev_school'] ?>','<?php echo $row['prev_school_city'] ?>','<?php echo $row['sibling1_name'] ?>','<?php echo $row['sibling1_grade'] ?>','<?php echo $row['sibling2_name'] ?>','<?php echo $row['sibling2_grade'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['g_occupation'] ?>','<?php echo $row['g_contact'] ?>','<?php echo $row['g_relation'] ?>','<?php echo $row['g_church_affil'] ?>','<?php echo $row['g_company'] ?>','<?php echo $row['g_phoneno'] ?>','<?php echo $row['g_email'] ?>','<?php echo $row['emergency_name'] ?>','<?php echo $row['e_relation'] ?>','<?php echo $row['e_contact'] ?>','<?php echo $row['e_address'] ?>','<?php echo $row['sickness'] ?>','<?php echo $row['sy'] ?>','<?php echo $row['stud_type'] ?>', true)"><span class="fa fa-edit"></span> Update
						</button>	
						<button class="btn btn-sm btn-danger" onclick="delete_student('<?php echo $row['id'] ?>','true')"><span class="fa fa-trash"></span> Delete</button>
						<button class="btn btn-sm btn-info" onclick="student_info('<?php echo $row['id'] ?>','<?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']?>')">
						<span class="fa fa-info"></span> View Info
						</button>
						<form action="print_student.php" method="post">
						<input type="text" style="display: none" id="print_id" name="print_id" value="<?php echo $row['id']; ?>" autocomplete="off"/>
						<button class="btn btn-dark btn-sm" type="submit" id="btn_submit"><span class="fa fa-print"></span> Print
						</button>  
						</form>
					</div>	
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Student Found!</td>';
   }

}else{

	if($sy == 0 and $grade == 0 and $section == ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and concat(t1.`ln`,t1.`mn`,t1.`fn`,t4.`grade_level`,t1.`stud_id`) like '%$search%';");
	}elseif($sy != 0 and $grade == '' and $section == '' ){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy' and concat(t1.`ln`,t1.`mn`,t1.`fn`,t4.`grade_level`,t1.`stud_id`) like '%$search%';");
	}
	elseif($sy != 0 and $grade != '' and $section == ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy' and t1.`grade_level` = '$grade' and concat(t1.`ln`,t1.`mn`,t1.`fn`,t4.`grade_level`,t1.`stud_id`) like '%$search%';");
	}
	elseif($sy != 0 and $grade != '' and $section != ''){
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and t1.`school_year` = '$sy' and t1.`grade_level` = '$grade' and t1.`section_id` = '$section' and concat(t1.`ln`,t1.`mn`,t1.`fn`,t4.`grade_level`,t1.`stud_id`) like '%$search%';");
	}else{
		$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id,t3.`id` as sy FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Enrolled' and concat(t1.`ln`,t1.`mn`,t1.`fn`,t4.`grade_level`,t1.`stud_id`) like '%$search%';");
	}
	
	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['stud_id']; ?></td>
				<td><?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']; ?></td>
				<td><?php echo $row['grade_level']?></td>
				<td><?php echo $row['section']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<div class="btn-group btn-justified text-center"> 
						<button class="btn btn-sm btn-default" onclick="update_student('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['birth_date'] ?>','<?php echo $row['age'] ?>','<?php echo $row['stud_address'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['grade_level'] ?>','<?php echo $row['language'] ?>','<?php echo $row['city'] ?>','<?php echo $row['father_name'] ?>','<?php echo $row['f_religion'] ?>','<?php echo $row['f_nationality'] ?>','<?php echo $row['f_educ_attain'] ?>','<?php echo $row['f_occupation'] ?>','<?php echo $row['f_contact'] ?>','<?php echo $row['mother_name'] ?>','<?php echo $row['m_religion'] ?>','<?php echo $row['m_nationality'] ?>','<?php echo $row['m_educ_attain'] ?>','<?php echo $row['m_occupation'] ?>','<?php echo $row['m_contact'] ?>','<?php echo $row['lives_with'] ?>','<?php echo $row['lives_with_other'] ?>','<?php echo $row['recommend_by'] ?>','<?php echo $row['prev_school'] ?>','<?php echo $row['prev_school_city'] ?>','<?php echo $row['sibling1_name'] ?>','<?php echo $row['sibling1_grade'] ?>','<?php echo $row['sibling2_name'] ?>','<?php echo $row['sibling2_grade'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['g_occupation'] ?>','<?php echo $row['g_contact'] ?>','<?php echo $row['g_relation'] ?>','<?php echo $row['g_church_affil'] ?>','<?php echo $row['g_company'] ?>','<?php echo $row['g_phoneno'] ?>','<?php echo $row['g_email'] ?>','<?php echo $row['emergency_name'] ?>','<?php echo $row['e_relation'] ?>','<?php echo $row['e_contact'] ?>','<?php echo $row['e_address'] ?>','<?php echo $row['sickness'] ?>','<?php echo $row['sy'] ?>','<?php echo $row['stud_type'] ?>', true)"><span class="fa fa-edit"></span> Update
						</button>	
						<button class="btn btn-sm btn-danger" onclick="delete_student('<?php echo $row['stud_id'] ?>','true')"><span class="fa fa-trash"></span> Delete</button>
						<button class="btn btn-sm btn-info" onclick="student_info('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']?>')">
						<span class="fa fa-info"></span> View Info
						</button>
						<form action="print_student.php" method="post">
						<input type="text" style="display: none" id="print_id" name="print_id" value="<?php echo $row['stud_id']; ?>" autocomplete="off"/>
						<button class="btn btn-dark btn-sm" type="submit" id="btn_submit"><span class="fa fa-print"></span> Print
						</button>  
						</form>
					</div>	
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Student Found!</td>';
   }

}

?>