<?php 
session_start();
include('../db_config.php');
date_default_timezone_set('Asia/Manila');
//page 1
//Student Info
$school_year = mysqli_real_escape_string($con, $_POST['school_year']);
$stud_type = mysqli_real_escape_string($con, $_POST['stud_type']);
$ln = mysqli_real_escape_string($con, $_POST['ln']);
$fn = mysqli_real_escape_string($con, $_POST['fn']);
$mn = mysqli_real_escape_string($con, $_POST['mn']);
$stud_id = mysqli_real_escape_string($con, $_POST['stud_id']);
$gender = mysqli_real_escape_string($con, $_POST['gender']);
$birth_date = mysqli_real_escape_string($con, $_POST['birth_date']);
$age = mysqli_real_escape_string($con, $_POST['age']);
$grade_level = mysqli_real_escape_string($con, $_POST['grade_level']);
$stud_address = mysqli_real_escape_string($con, $_POST['stud_address']);
$city = mysqli_real_escape_string($con, $_POST['city']);
$language = mysqli_real_escape_string($con, $_POST['language']);

//father info
$father_name = mysqli_real_escape_string($con, $_POST['father_name']);
$f_religion = mysqli_real_escape_string($con, $_POST['f_religion']);
$f_nationality = mysqli_real_escape_string($con, $_POST['f_nationality']);
$f_educ_attain = mysqli_real_escape_string($con, $_POST['f_educ_attain']);
$f_occupation = mysqli_real_escape_string($con, $_POST['f_occupation']);
$f_contact = mysqli_real_escape_string($con, $_POST['f_contact']);

//mother info
$mother_name = mysqli_real_escape_string($con, $_POST['mother_name']);
$m_religion = mysqli_real_escape_string($con, $_POST['m_religion']);
$m_nationality = mysqli_real_escape_string($con, $_POST['m_nationality']);
$m_educ_attain = mysqli_real_escape_string($con, $_POST['m_educ_attain']);
$m_occupation = mysqli_real_escape_string($con, $_POST['m_occupation']);
$m_contact = mysqli_real_escape_string($con, $_POST['m_contact']);

//page 2
$lives_with = mysqli_real_escape_string($con, $_POST['lives_with']);
$lives_with_other = mysqli_real_escape_string($con, $_POST['lives_with_other']);
$recommend_by = mysqli_real_escape_string($con, $_POST['recommend_by']);
$prev_school = mysqli_real_escape_string($con, $_POST['prev_school']);
$prev_school_city = mysqli_real_escape_string($con, $_POST['prev_school_city']);
$sibling1_name = mysqli_real_escape_string($con, $_POST['sibling1_name']);
$sibling1_grade = mysqli_real_escape_string($con, $_POST['sibling1_grade']);
$sibling2_name = mysqli_real_escape_string($con, $_POST['sibling2_name']);
$sibling2_grade = mysqli_real_escape_string($con, $_POST['sibling2_grade']);
$guardian_name = mysqli_real_escape_string($con, $_POST['guardian_name']); 
$g_relation = mysqli_real_escape_string($con, $_POST['g_relation']);
$g_church_affil = mysqli_real_escape_string($con, $_POST['g_church_affil']);
$g_occupation = mysqli_real_escape_string($con, $_POST['g_occupation']);
$g_company = mysqli_real_escape_string($con, $_POST['g_company']);
$g_contact = mysqli_real_escape_string($con, $_POST['g_contact']);
$g_phoneno = mysqli_real_escape_string($con, $_POST['g_phoneno']);
$g_email = mysqli_real_escape_string($con, $_POST['g_email']);
$emergency_name = mysqli_real_escape_string($con, $_POST['emergency_name']);
$e_relation = mysqli_real_escape_string($con, $_POST['e_relation']);
$e_contact = mysqli_real_escape_string($con, $_POST['e_contact']);
$e_address = mysqli_real_escape_string($con, $_POST['e_address']);
$sickness = mysqli_real_escape_string($con, $_POST['sickness']);
$date_save = date('Y-m-d H:i:s');

$check_id = mysqli_query($con, "SELECT * from tbl_enrollment_form where stud_id = '$stud_id'");
if (mysqli_num_rows($check_id)>0) {
	//update
	$sql = mysqli_query($con, "UPDATE tbl_enrollment_form set stud_id = '$stud_id',ln = '$ln',fn = '$ln',mn = '$mn',birth_date = '$birth_date',age= '$age',stud_address= '$stud_address',gender= '$gender',grade_level = '$grade_level',language= '$language',city= '$city',father_name = '$father_name',f_religion = '$f_religion',f_nationality= '$f_nationality',f_educ_attain = '$f_educ_attain',f_occupation = '$f_occupation',f_contact = '$f_contact',mother_name = '$mother_name',m_religion = '$m_religion',m_nationality = '$m_nationality',m_educ_attain = '$m_educ_attain',m_occupation = '$m_occupation',m_contact = '$m_contact',lives_with = '$lives_with',lives_with_other = '$lives_with_other',recommend_by = '$recommend_by',prev_school = '$prev_school',prev_school_city = '$prev_school_city',sibling1_name = '$sibling1_name',sibling1_grade = '$sibling1_grade',sibling2_name = '$sibling2_name',sibling2_grade = '$sibling2_grade',guardian_name = '$guardian_name',g_occupation = '$g_occupation',g_contact = '$g_contact',g_relation = '$g_relation',g_church_affil = '$g_church_affil',g_company = '$g_company',g_phoneno = '$g_phoneno',g_email = '$g_email',emergency_name = '$emergency_name',e_relation = '$e_relation',e_contact = '$e_contact',e_address = '$e_address',sickness = '$sickness',school_year = '$school_year',stud_type = '$stud_type' Where stud_id = '$stud_id'");
	if($sql){
		echo 2;
	}
	
}else{
	//insert

	$sql = mysqli_query($con, "INSERT INTO tbl_enrollment_form(stud_id,ln,fn,mn,birth_date,age,stud_address,gender,grade_level,language,city,father_name,f_religion,f_nationality,f_educ_attain,f_occupation,f_contact,mother_name,m_religion,m_nationality,m_educ_attain,m_occupation,m_contact,lives_with,lives_with_other,recommend_by,prev_school,prev_school_city,sibling1_name,sibling1_grade,sibling2_name,sibling2_grade,guardian_name,g_occupation,g_contact,g_relation,g_church_affil,g_company,g_phoneno,g_email,emergency_name,e_relation,e_contact,e_address,sickness,date_save,school_year,stud_type) VALUES('$stud_id','$ln','$fn','$mn','$birth_date','$age','$stud_address','$gender','$grade_level','$language','$city','$father_name','$f_religion','$f_nationality','$f_educ_attain','$f_occupation','$f_contact','$mother_name','$m_religion','$m_nationality','$m_educ_attain','$m_occupation','$m_contact','$lives_with','$lives_with_other','$recommend_by','$prev_school','$prev_school_city','$sibling1_name','$sibling1_grade','$sibling2_name','$sibling2_grade','$guardian_name','$g_occupation','$g_contact','$g_relation', '$g_church_affil','$g_company','$g_phoneno','$g_email', '$emergency_name','$e_relation','$e_contact','$e_address','$sickness','$date_save','$school_year','$stud_type')");

	if($sql){
		echo 1;
	}

}

?>