<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search_sub']);

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT * from tbl_subjects");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['subject_code'];?></td>
				<td><?php echo $row['subject_name']; ?></td>
				<td><?php echo $row['stud_type']; ?></td>
				<td>
					<button class="btn btn-sm btn-default" onclick="edit_sub('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_name'] ?>','<?php echo $row['stud_type'] ?>');"><span class="fa fa-edit"></span>Edit</button>
					<button class="btn btn-sm btn-danger" onclick="delete_sub('<?php echo $row['id'] ?>')"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Record Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT * from tbl_subjects WHERE concat(subject_code,subject_name,stud_type) like '%$search%';");


	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['subject_code'];?></td>
				<td><?php echo $row['subject_name']; ?></td>
				<td><?php echo $row['stud_type']; ?></td>
				<td>
					<button class="btn btn-sm btn-default" onclick="edit_sub('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_name'] ?>','<?php echo $row['stud_type'] ?>');"><span class="fa fa-edit"></span>Edit</button>
					<button class="btn btn-sm btn-danger" onclick="delete_sub('<?php echo $row['id'] ?>')"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Record Found!</td>';
   }

}

?>