<!-- Student -->
<div class="modal fade" id="student_form">
    <div class="modal-dialog modal-large">
	    <div class="modal-content">
	        <!-- Modal Header -->
	        <div class="modal-header">
	          <p class="title"><span class="fa fa-user-plus"></span> New Student</p>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <input type="text" id="is_enroll">
	        <!-- Modal body -->
	        <div class="modal-body" autocomplete="on">
	        	<div class="container-fluid">
	        		<ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item ">
                        <a class="nav-link active" data-toggle="tab" href="#page1"><span class="fa fa-circle"></span> Page 1</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#page2"><span class="fa fa-circle"></span> Page 2</a>
                      </li>
                      <li class="nav-item col-lg-3">
                        	<div class="form-group">
					            <select id="school_year" class="form-control select_form">
					            	<option selected="" disabled="" value="0">Select School Year</option>
					            	<?php echo school_year($con); ?>
					            </select>
					         </div>
                      </li>
                      <li class="nav-item col-lg-3">
                        	<div class="form-group">
					            <select id="stud_type" class="form-control select_form">
					            	<option selected="" disabled="" value="none">Select Student Type</option>
					            	<option value="ELEMENTARY">Elementary</option>
					            	<option value="HIGHSCHOOL">Highschool</option>
					            </select>
					         </div>
                      </li>
                      <li class="nav-item col-lg-1">
                      	
                      </li>
                       <li class="nav-item">
                        	<button class="btn btn-success" onclick="save_update_student();"><span class="fa fa-save"></span> Save Student</button>
                        	<button class="btn btn-danger" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
                        
                      </li>
                    </ul>

                    <div class="row">
  					    <div class="col-md-12">
                          <div class="tab-content">
                          	<!-- Page 1 -->
                            <div id="page1" class="tab-pane active">
                              	<div class="row">
                              		<div class="col-lg-12">
                              			<div class="container-fluid">
	                              			<div class="card">
												<div class="card-header bg-info text-white">
													<span class="fa fa-user"></span> Student Information
												</div>
												<div class="card-body bg-light">
													<div class="col-lg-12 row">
														<div class="col-lg-3">
															<div class="form-group">
																<label for="ln" class="w3-small">Lastname:</label>
													            <input class="form-control" id="ln" name="ln" type="text" placeholder="Enter Lastname">
													         </div>
											     		</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="fn" class="w3-small">Firstname:</label>
													            <input class="form-control" id="fn" name="fn" type="text" placeholder="Enter Firstname">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="mn" class="w3-small">Middlename:</label>
													            <input class="form-control" id="mn" name="mn" type="text" placeholder="Enter Middlename">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="stud_id" class="w3-small">Student ID:</label>
													            <input class="form-control" disabled="" id="stud_id" name="stud_id" type="text" placeholder="Student ID">
													         </div>
														</div>
													</div>

													<div class="col-lg-12 row">
														<div class="col-lg-3">
															<div class="form-group">
																<label for="ln" class="w3-small">Gender:</label>
													            <select id="gender" class="form-control select_form">
													            	<option selected="" value="NONE">Select Gender</option>
													            	<option value="MALE">MALE</option>
													            	<option value="FEMALE">FEMALE</option>
													            </select>
													         </div>
											     		</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="birth_date" class="w3-small">Date of Birth:</label>
													            <input class="form-control" id="birth_date" name="birth_date" type="date" oninput="get_age();">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="age" class="w3-small">Age:</label>
													            <input class="form-control" disabled="" id="age" name="age" type="text" placeholder="Age">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="grade_level" class="w3-small">Grade Level:</label>
													            <select id="grade_level" class="form-control select_form">
													            	<?php echo get_grade_level($con); ?>
													            </select>
													         </div>
														</div>
													</div>

													<div class="col-lg-12 row">
														<div class="col-lg-5">
															<div class="form-group">
																<label for="stud_address" class="w3-small">Home Address:</label>
													            <input class="form-control" type="text" id="stud_address" name="stud_address" placeholder="Enter Full Address...">
													         </div>
											     		</div>
														<div class="col-lg-4">
															<div class="form-group">
																<label for="city" class="w3-small">City:</label>
													            <input class="form-control" id="city" name="city" type="text" placeholder="Enter City">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="language" class="w3-small">Language Spoken:</label>
													            <input class="form-control" id="language" name="language" type="text" placeholder="Enter Language Spoken">
													         </div>
														</div>
													</div>
													
												</div>
											</div>

											<div class="card">
												<div class="card-header bg-info text-white">
													<span class="fa fa-user"></span> Parent's Information
												</div>
												<div class="card-body bg-light">
													<div class="col-lg-12 row">
														<div class="col-lg-6">
															<div class="card">
																<div class="card-header bg-primary text-white">
																	Father's Information
																</div>
																<div class="card-body">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label for="father_name" class="w3-small">Father's Name:</label>
																            <input class="form-control" id="father_name" name="father_name" type="text" placeholder="Enter Father's Name">
																         </div>
																         <div class="form-group">
																			<label for="f_religion" class="w3-small">Religion:</label>
																            <input class="form-control" id="f_religion" name="f_religion" type="text" placeholder="Enter Religion">
																         </div>
																         <div class="form-group">
																			<label for="f_nationality" class="w3-small">Nationality:</label>
																            <input class="form-control" id="f_nationality" name="f_nationality" type="text" placeholder="Enter Nationality">
																         </div>
																         <div class="form-group">
																			<label for="f_educ_attain" class="w3-small">Educational Attainment:</label>
																            <input class="form-control" id="f_educ_attain" name="f_educ_attain" type="text" placeholder="Enter Educational Attainment">
																         </div>
																         <div class="form-group">
																			<label for="f_occupation" class="w3-small">Occupation:</label>
																            <input class="form-control" id="f_occupation" name="f_occupation" type="text" placeholder="Enter Occupation">
																         </div>
																         <div class="form-group">
																			<label for="f_contact" class="w3-small">Telephone/Cp No.:</label>
																            <input class="form-control" id="f_contact" name="f_contact" type="text" placeholder="Enter Contact Number">
																         </div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="card">
																<div class="card-header bg-primary text-white">
																	Mother's Information
																</div>
																<div class="card-body">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label for="mother_name" class="w3-small">Mother's Name:</label>
																            <input class="form-control" id="mother_name" name="mother_name" type="text" placeholder="Enter Mother's Name">
																         </div>
																         <div class="form-group">
																			<label for="m_religion" class="w3-small">Religion:</label>
																            <input class="form-control" id="m_religion" name="m_religion" type="text" placeholder="Enter Religion">
																         </div>
																         <div class="form-group">
																			<label for="m_nationality" class="w3-small">Nationality:</label>
																            <input class="form-control" id="m_nationality" name="m_nationality" type="text" placeholder="Enter Nationality">
																         </div>
																         <div class="form-group">
																			<label for="m_educ_attain" class="w3-small">Educational Attainment:</label>
																            <input class="form-control" id="m_educ_attain" name="m_educ_attain" type="text" placeholder="Enter Educational Attainment">
																         </div>
																         <div class="form-group">
																			<label for="m_occupation" class="w3-small">Occupation:</label>
																            <input class="form-control" id="m_occupation" name="m_occupation" type="text" placeholder="Enter Occupation">
																         </div>
																         <div class="form-group">
																			<label for="m_contact" class="w3-small">Telephone/Cp No.:</label>
																            <input class="form-control" id="m_contact" name="m_contact" type="text" placeholder="Enter Contact Number">
																         </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
                              			</div>
                              		</div>
                              	</div> 
                            </div>
                            <!-- Page 1 -->

                            <!-- Page 2 -->
                            <div id="page2" class="tab-pane">
                            	<div class="row">
                            		<div class="col-lg-12">
                            			<div class="container-fluid">
                            				<div class="card">
                            					<div class="card-header bg-info text-white">
                            						<span class="fa fa-info"></span> Other Information
                            					</div>
                            					<div class="card-body">
                            						<div class="col-lg-12 row">
                            							<div class="col-lg-7 row">
                            								<div class="col-lg-6">
	                            								<div class="form-group">
																	<label for="ln" class="w3-small">Student's Lives with(Select One):
																	</label>
														            <select id="lives_with" class="form-control select_form">
														            	<option selected="" value="NONE">Select One</option>
														            	<option value="Both Parent">Both Parent</option>
														            	<option value="Mother Only">Mother Only</option>
														            	<option value="Father Only">Father Only</option>
														            </select>
														         </div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="lives_with_other" class="w3-small">Some Other Than Parent:</label>
														            <input class="form-control" id="lives_with_other" name="lives_with_other" type="text" placeholder="Enter Some Other Than Parent">
													         	</div>
                            								</div>
                            							</div>
                            							<div class="col-lg-5">
                            								<div class="form-group">
																<label for="recommend_by" class="w3-small">If the Student is new to Galilee Integraded School recommended by?:</label>
													            <input class="form-control" id="recommend_by" name="recommend_by" type="text" placeholder="Enter Recommended by.">
												         	</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12 row">
                            							<div class="col-lg-6">
                        									<div class="form-group">
																<label for="prev_school" class="w3-small">Previous School Attended:</label>
													            <input class="form-control" id="prev_school" name="prev_school" type="text" placeholder="Enter School">
												         	</div>
                        								</div>
                        								<div class="col-lg-6">
                        									<div class="form-group">
																<label for="prev_school_city" class="w3-small">City:</label>
													            <input class="form-control" id="prev_school_city" name="prev_school_city" type="text" placeholder="Enter City">
												         	</div>
                        								</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Sibling Attending Galilee Integrated School in the same School Year:</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling1_name" class="w3-small">Name:</label>
														            <input class="form-control" id="sibling1_name" name="sibling1_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling1_grade" class="w3-small">Grade:
																	</label>
														            <select id="sibling1_grade" class="form-control select_form">
														            	<?php echo get_grade_level($con); ?>
														            </select>
														         </div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling2_name" class="w3-small">Name:</label>
														            <input class="form-control" id="sibling2_name" name="sibling2_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling2_grade" class="w3-small">Grade:
																	</label>
														            <select id="sibling2_grade" class="form-control select_form">
														            	<?php echo get_grade_level($con); ?>
														            </select>
														         </div>
                            								</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Parent/Guardian:</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="guardian_name" class="w3-small">Name:</label>
														            <input class="form-control" id="guardian_name" name="guardian_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_relation" class="w3-small">Relationship:</label>
														            <input class="form-control" id="g_relation" name="g_relation" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_church_affil" class="w3-small">Church Affiliation:</label>
														            <input class="form-control" id="g_church_affil" name="g_church_affil" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="g_occupation" class="w3-small">Occupation:</label>
														            <input class="form-control" id="g_occupation" name="g_occupation" type="text" placeholder="Enter Occupation">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="g_company" class="w3-small">Company Name:
																	</label>
														            <input class="form-control" id="g_company" name="g_company" type="text" placeholder="Enter Company">
														         </div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_contact" class="w3-small">Cellphone:</label>
														            <input class="form-control" id="g_contact" name="g_contact" type="text" placeholder="Enter Cellphone No.">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_phoneno" class="w3-small">Work Phone:</label>
														            <input class="form-control" id="g_phoneno" name="g_phoneno" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_email" class="w3-small">Email</label>
														            <input class="form-control" id="g_email" name="g_email" type="text" placeholder="Enter Email">
													         	</div>
                            								</div>
                            							</div>
                            						
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>In the case of an emergency when parents/guardian cannot be reached, Contact::</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-3">
                            									<div class="form-group">
																	<label for="emergency_name" class="w3-small">Name:</label>
														            <input class="form-control" id="emergency_name" name="emergency_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-3">
                            									<div class="form-group">
																	<label for="e_relation" class="w3-small">Relationship:</label>
														            <input class="form-control" id="e_relation" name="e_relation" type="text" placeholder="Enter Relationship">
													         	</div>
                            								</div>
                            								<div class="col-lg-2">
                            									<div class="form-group">
																	<label for="e_contact" class="w3-small">Contact:</label>
														            <input class="form-control" id="e_contact" name="e_contact" type="text" placeholder="Enter Contact">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="e_address" class="w3-small">Address:</label>
														            <input class="form-control" id="e_address" name="e_address" type="text" placeholder="Enter Address">
													         	</div>
                            								</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Please Check if the student has the following sickness:</b></label>
                            							<input type="hidden" id="sickness">
                            							<div class="col-lg-12 row" id="load_sickness">
                            								
                            							</div>
                            						</div>
                            					</div>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                            </div>
                            <!-- Page 2 -->
                          </div>

  					     </div>
					</div>

	        	</div>
	        </div>
	    </div>
	</div>
</div>
</div>