<?php 
session_start();
include('../db_config.php');
$sy = $_POST['p_sy2'];
$grade = $_POST['p_grade2'];

$sql_sy = mysqli_query($con, "SELECT school_year from tbl_school_year where id ='$sy'");
$row_sy = mysqli_fetch_assoc($sql_sy);

$sql_grade = mysqli_query($con, "SELECT grade_level from tbl_grade_level where id ='$grade'");
$row_grade = mysqli_fetch_assoc($sql_grade);

$sql_count = mysqli_query($con, "SELECT count(id) from tbl_enrollment_form Where school_year = '$sy' and grade_level = '$grade' and status = 'Enrolled'");
$row_count = mysqli_fetch_assoc($sql_count);

$sql_count_boys = mysqli_query($con, "SELECT count(id) from tbl_enrollment_form Where school_year = '$sy' and grade_level = '$grade' and status = 'Enrolled' and gender='MALE'");
$row_boys = mysqli_fetch_assoc($sql_count_boys);

$sql_count_girls = mysqli_query($con, "SELECT count(id) from tbl_enrollment_form Where school_year = '$sy' and grade_level = '$grade' and status = 'Enrolled' and gender='FEMALE'");
$row_girls = mysqli_fetch_assoc($sql_count_girls);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Student List</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/print.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="registrar.js"></script>
</head>
<body>
	<div class="col-lg-12 text-center">
		<img src="../img/logo.jpg" class="img-responsive" width="80px" height="80px">
		<h4>Galilee Integraded School</h4>
		<h5>Student List</h5>
		<hr>
		<div class="col-lg-12 row">
			<div class="col-lg-1"></div>
			<div class="col-lg-7">
				<table>
					<tr>
						<td class="text-right">Grade: &nbsp;</td>
						<td class="text-left"><b><?php echo $row_grade['grade_level']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">No. of Students: &nbsp;</td>
						<td class="text-left"><b><?php echo $row_count['count(id)']; ?></b></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4 float-right">
				<table>
					<tr>
						<td class="text-right">Date Print: &nbsp;</td>
						<td class="text-left"><b><?php echo date('Y-m-d'); ?></b></td>
					</tr>
					<tr>
						<td class="text-right">School Year: &nbsp;</td>
						<td class="text-left"><b><?php echo $row_sy['school_year']; ?></b></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<!-- Boys -->
		<div class="col-lg-12 row">
			<div class="col-lg-1"></div>
			<div class="col-lg-5" style="margin-right: 5px;">
				<strong>Boys: <?php echo $row_boys['count(id)']; ?></strong>
				<table class="table table-bordered table-sm">
					<thead>
						<tr>
							<th>No.</th>
							<th class="text-left">Name</th>	
							<th class="text-left">Section</th>
						</tr>					
					</thead>
					<tbody>
						<?php
							$sql_boys = mysqli_query($con, "SELECT t1.*,t2.`section` FROM tbl_enrollment_form t1 LEFT JOIN tbl_section t2 on t1.`section_id` = t2.`id` where t1.`school_year` = '$sy' and t1.`grade_level` = '$grade' and t1.`status` = 'Enrolled' and t1.`gender` = 'MALE' ORDER by t1.`ln` ASC");
							$counter = 0;
							if(mysqli_num_rows($sql_boys)){

								while ($row = mysqli_fetch_assoc($sql_boys)) { $counter ++;?>

									<tr>
										<td><?php echo $counter;?>.</td>
										<td class="text-left"><?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']; ?></td>
										<td class="text-left"><?php echo $row['section']; ?></td>
										
									</tr>

								<?php }

							}else{
								echo '<td colspan="2">No Student Found!</td>';
						   }

						?>
					</tbody>
				</table>
			</div>
			<div class="col-lg-5" style="margin-left: 5px;">
				<strong>Girls: <?php echo $row_girls['count(id)']; ?></strong>
				<table class="table table-bordered table-sm">
					<thead>
						<tr>
							<th>No.</th>
							<th class="text-left">Name</th>	
							<th class="text-left">Section</th>	
						</tr>					
					</thead>
					<tbody>
						<?php
							$sql_girls = mysqli_query($con, "SELECT t1.*,t2.`section` FROM tbl_enrollment_form t1 LEFT JOIN tbl_section t2 on t1.`section_id` = t2.`id` where t1.`school_year` = '$sy' and t1.`grade_level` = '$grade' and t1.`status` = 'Enrolled' and t1.`gender` = 'FEMALE' ORDER by t1.`ln` ASC");
							$counter = 0;
							if(mysqli_num_rows($sql_girls)){

								while ($row2 = mysqli_fetch_assoc($sql_girls)) { $counter ++;?>

									<tr>
										<td><?php echo $counter;?>.</td>
										<td class="text-left"><?php echo $row2['ln'].',  '. $row2['fn'].' '. $row2['mn']; ?></td>
										<td class="text-left"><?php echo $row2['section']; ?></td>
										
									</tr>

								<?php }

							}else{
								echo '<td colspan="2">No Student Found!</td>';
						   }

						?>
					</tbody>
				</table>
			</div>
			<div class="col-lg-1"></div>
		</div>
		<br><br>
		<div class="col-lg-12 text-center" id="btns">
			<button class="btn btn-sm btn-default" onclick="print_preview();"><span class="fa fa-print"></span> Preview</button>
			<button class="btn btn-sm btn-default" onclick="window.location='studentlist.php'"> Back</button>
		</div>
	</div>
	<script type="text/javascript">
	function print_preview(){
		$('#btns').addClass('none');
		window.print();
		$('#btns').removeClass('none');
	}
</script>	
</body>
</html>