<?php 
session_start();
include('../db_config.php');
$stud_id = mysqli_real_escape_string($con, $_POST['s_id']);
$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`id` = '$stud_id'");
$row = mysqli_fetch_assoc($sql);
?>
<ul class="list-group">
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Student ID: </span>
	<span class="text-default">
	<b><?php echo $row['stud_id']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Student Type: </span>
	<span class="text-default">
	<b><?php echo $row['stud_type']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Grade: </span>
	<span class="text-default">
	<b><?php echo $row['grade_level']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Section: </span>
	<span class="text-default">
	<b><?php echo $row['section']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Age: </span>
	<span class="text-default">
	<b><?php echo $row['age']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Gender: </span>
	<span class="text-default">
	<b><?php echo $row['gender']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Address: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['stud_address'] == ''){
			echo 'NONE';
		}else{
			echo $row['stud_address']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Language Spoken: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['language'] == ''){
			echo 'NONE';
		}else{
			echo $row['language']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Father: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['father_name'] == ''){
			echo 'NONE';
		}else{
			echo $row['father_name']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Mother: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['mother_name'] == ''){
			echo 'NONE';
		}else{
			echo $row['mother_name']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Lives with: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['lives_with'] == ''){
			echo 'NONE';
		}else{
			echo $row['lives_with']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Sickness: </span>
	<span class="text-default">
	<b>
		<?php 
		if($row['sickness'] == ''){
			echo 'NONE';
		}else{
			echo $row['sickness']; 
		}
		?>
	</b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Status: </span>
	<span class="text-default">
	<b class="text-success"><?php echo $row['status']; ?></b>
	</span>
	</li>
	<li class="list-group-item d-flex justify-content-between align-items-center w3-hover-shadow">
	<span class="text-default">Date Enrolled: </span>
	<span class="text-default">
	<b><?php echo date('M d, Y', strtotime($row['date_enrolled'])); ?></b>
	</span>
	</li>

</ul>