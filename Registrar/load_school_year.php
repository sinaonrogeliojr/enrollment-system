<?php 
session_start();
include('../db_config.php');
	$sql = mysqli_query($con, "SELECT * from tbl_school_year");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<?php 
						if($row['status'] == 'ACTIVE'){ ?>
							<span class="badge badge-success"><?php echo $row['status']; ?></span>
						<?php }else{ ?>

							<?php 
							if($row['has_data'] == 1){ ?>

								<span class="badge badge-danger"><?php echo $row['status']; ?></span>


							<?php }else{ ?>

								<span class="badge badge-danger" onclick="set_active('<?php echo $row['id'] ?>', '<?php echo $row['school_year'] ?>')"><?php echo $row['status']; ?></span>

							<?php } ?>

												<?php
						}
					?>

				</td>
				<td>

					

					<button class="btn btn-sm btn-default" onclick="update_sy('<?php echo $row['id']?>','<?php echo $row['school_year'] ?>')">
							<span class="fa fa-edit"></span> Update
						</button>
					<button class="btn btn-sm btn-danger" onclick="delete_sy('<?php echo $row['id'] ?>')">
						<span class="fa fa-trash"></span> Delete
					</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Student Found!</td>';
   }
?>