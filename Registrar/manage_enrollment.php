<?php 
session_start();
if (!isset($_SESSION['registrar'])) {
	@header('location:../');
}

include('../db_config.php');
function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade_level");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['grade_level'].'</option>';
        }
    }
}
function school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by status ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registrar</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="registrar.js"></script>
</head>
<body onload="load_school_year(); list_sections(); list_subjects();">
<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System</strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu " onclick="window.location='enrollment.php'"><span class="fa fa-sign-in fa-2x"></span> Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='manage_enrollment.php'"><span class="fa fa-edit fa-2x"></span> Manage Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='studentlist.php'"><span class="fa fa-users fa-2x"></span> Student List</button>
					<!--<button class="btn btn-lg btn-success btn-menu"><span class="fa fa-gears fa-2x"></span> Admin Options</button> -->
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	
	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Manage Enrollement</p>
				</div>
			</div><br>
			<ul class="nav nav-tabs" role="tablist">
              <li class="nav-item ">
                <a class="nav-link active" data-toggle="tab" href="#school_year"><span class="fa fa-circle"></span> School Year</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#section"><span class="fa fa-circle"></span> Sections</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#subject"><span class="fa fa-circle"></span> Subjects</a>
              </li>
            </ul><hr>
            <div class="tab-content">
              	<!-- Manage School Year -->
                <div id="school_year" class="tab-pane active">
                  	<div class="row">
                  		<div class="col-lg-12">
                  			<div class="container-fluid">
                  				<div class="row">
									<div class="col-lg-6 input">
										<button class="btn btn-dark" data-toggle="modal" data-target="#add_update_sy"><span class="fa fa-plus"></span> Add School Year</button>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="table table-responsive">
										<table class="table table-striped table-hover table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>School Year</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="load_school_year">
												
											</tbody>
										</table>
									</div>
								</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
                <!-- Manage Sections -->
                <div id="section" class="tab-pane">
                  	<div class="row">
                  		<div class="col-lg-12">
                  			<div class="container-fluid">
                  				<div class="row">
									<div class="col-lg-2 input">
										<button class="btn btn-dark" data-toggle="modal" data-target="#add_update_sec"><span class="fa fa-plus"></span> Add Section</button>
									</div>
									<div class="col-lg-10">
										<div class="input-group">
							                <input type="show" class="form-control" oninput="list_sections();" name="search_sec" id="search_sec" placeholder="Search Section...">
							                <span class="input-group-addon"><i class="fa fa-search"></i></span>
							            </div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="table table-responsive">
										<table class="table table-striped table-hover table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>Section</th>
													<th>Grade</th>
													<th>School Year</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="load_sections">
												
											</tbody>
										</table>
									</div>
								</div>
                  			</div>
                  		</div>
                  	</div>
                </div>

                <!-- Manage Subjects -->
                <div id="subject" class="tab-pane">
                  	<div class="row">
                  		<div class="col-lg-12">
                  			<div class="container-fluid">
                  				<div class="row">
									<div class="col-lg-2 input">
										<button class="btn btn-dark" data-toggle="modal" data-target="#add_update_sub"><span class="fa fa-plus"></span> Add Subject</button>
									</div>
									<div class="col-lg-10">
										<div class="input-group">
							                <input type="show" class="form-control" oninput="list_subjects();" name="search_sub" id="search_sub" placeholder="Search Section...">
							                <span class="input-group-addon"><i class="fa fa-search"></i></span>
							            </div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="table table-responsive">
										<table class="table table-striped table-hover table-sm">
											<thead>
												<tr>
													<th>Subject ID</th>
													<th>Subject Code</th>
													<th>Subject Name</th>
													<th>Type</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="load_subjects">
												
											</tbody>
										</table>
									</div>
								</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
		</div>
	</div>
</div>
<!-- School Year -->
<div class="modal fade" id="add_update_sy">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title" id="sy"><span class="fa fa-plus"></span> Add School Year</p>
				<input type="hidden" id="sch_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-lg-12">
					<div class="form-group">
						<label for="stud_address" class="w3-small">School Year:</label>
			            <input class="form-control" type="text" id="new_sy" name="new_sy" placeholder="Enter School Year...">
			         </div>
				</div>
			</div>
			<div class="modal-footer" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-success" id="save_sy" onclick="save_sy();"><span class="fa fa-save"></span> Save</button>
			</div>
		</div>
	</div>
</div>

<!-- Section -->
<div class="modal fade" id="add_update_sec">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title" id="sec"><span class="fa fa-plus"></span> Add Section</p>
				<input type="hidden" id="sec_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid" style="margin-top: 10px;">
					<div class="col-lg-12">
					<div class="form-group">
						<label for="stud_address" class="w3-small">School Year:</label>
			            <select id="sec_sy" class="form-control select_form">
			            	<option value="none" selected="" disabled="">--Select School Year--</option>
			            	<?php echo school_year($con); ?>
			            </select>
			         </div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<label for="stud_address" class="w3-small">Grade Level:</label>
			            <select id="sec_grade" class="form-control select_form">
			            	<option value="none" selected="" disabled="">--Select Grade Level--</option>
			            	<?php echo get_grade_level($con); ?>
			            </select>
			         </div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<label for="stud_address" class="w3-small">Section</label>
			            <input class="form-control" type="text" id="new_sec" name="new_sec" placeholder="Enter Section...">
			         </div>
				</div>
				</div>
				
			</div>
			<div class="modal-footer" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-success" id="save_sec" onclick="save_sec();"><span class="fa fa-save"></span> Save</button>
			</div>
		</div>
	</div>
</div>

<!-- Section -->
<div class="modal fade" id="add_update_sub">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title" id="sub"><span class="fa fa-plus"></span> Add Subject</p>
				<input type="hidden" id="sub_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid" style="margin-top: 10px;">
					<div class="col-lg-12">
					<div class="form-group">
						<label for="sub_type" class="w3-small">Type</label>
			            <select id="sub_type" class="form-control select_form">
			            	<option value="none" selected="" disabled="">--Select Student Type--</option>
			            	<option value="ELEMENTARY">ELEMENTARY</option>
			            	<option value="HIGHSCHOOL">HIGHSCHOOL</option>
			            </select>
			         </div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<label for="sub_code" class="w3-small">Subject Code</label>
			            <input class="form-control" type="text" id="sub_code" name="new_sec" placeholder="Enter Subject Code...">
			         </div>
				</div>
				<div class="col-lg-12">
					<div class="form-group">
						<label for="new_sub" class="w3-small">Subject Name</label>
			            <input class="form-control" type="text" id="new_sub" name="new_sec" placeholder="Enter Subject Name...">
			         </div>
				</div>
				</div>
				
			</div>
			<div class="modal-footer" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-success" id="save_sub" onclick="save_sub();"><span class="fa fa-save"></span> Save</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>