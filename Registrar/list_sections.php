<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search_sec']);

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT t1.`id`,t1.`section`,t2.`grade_level`,t3.`school_year`,t2.id as g_id,t3.id as s_id FROM tbl_section t1 
LEFT JOIN tbl_grade_level t2 ON t1.`grade` = t2.`id`
LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`type`=1");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['section'];?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<button class="btn btn-sm btn-default" onclick="edit_sec('<?php echo $row['id'] ?>','<?php echo $row['section'] ?>','<?php echo $row['g_id'] ?>','<?php echo $row['s_id'] ?>');"><span class="fa fa-edit"></span>Edit</button>
					<button class="btn btn-sm btn-danger" onclick="delete_sec('<?php echo $row['id'] ?>')"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Record Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT t1.`id`,t1.`section`,t2.`grade_level`,t3.`school_year` FROM tbl_section t1 
LEFT JOIN tbl_grade_level t2 ON t1.`grade` = t2.`id`
LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`type`=1 and concat(t1.section,t3.school_year,t2.grade_level) like '%$search%';");


	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['id']; ?></td>
				<td><?php echo $row['section'];?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<button class="btn btn-sm btn-default" onclick="edit_sec('<?php echo $row['id'] ?>','<?php echo $row['section'] ?>','<?php echo $row['g_id'] ?>','<?php echo $row['s_id'] ?>');"><span class="fa fa-edit"></span>Edit</button>
					<button class="btn btn-sm btn-danger" onclick="delete_sec('<?php echo $row['id'] ?>')"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Record Found!</td>';
   }

}

?>