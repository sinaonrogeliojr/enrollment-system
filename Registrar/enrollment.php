<?php 
session_start();
if (!isset($_SESSION['registrar'])) {
	@header('location:../');
}

include('../db_config.php');
function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade_level");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['grade_level'].'</option>';
        }
    }
}
function school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year where status = 'ACTIVE'");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registrar</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="registrar.js"></script>
  	<script type="text/javascript">
  		function count_check(){
			var chks = $('.select_accounts').filter(':checked').length

			if(chks > 0){
				document.getElementById('btn-update').disabled= false;
			}else{
				document.getElementById('btn-update').disabled= true;
			}
		}
  	</script>
</head>
<body onload="load_students(); load_sickness(); generate_id(); load_old_students();">
<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System</strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='enrollment.php'"><span class="fa fa-sign-in fa-2x"></span> Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='manage_enrollment.php'"><span class="fa fa-edit fa-2x"></span> Manage Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='studentlist.php'"><span class="fa fa-users fa-2x"></span> Student List</button>
					<!--<button class="btn btn-lg btn-success btn-menu"><span class="fa fa-gears fa-2x"></span> Admin Options</button> -->
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	

	<br>
					<div class="row">
						<div class="col-lg-4 form-inline">
							<a data-toggle="modal" data-target="#student_form"><button type="button" class="btn btn-dark btn-normal"><span class="fa fa-user-plus fa-lg"></span> Student Form</button></a>
							<!-- <a data-toggle="modal" data-target="#generate_students"><button type="button" class="btn btn-dark btn-normal"><span class="fa fa-refresh fa-lg"></span> Generate Old Students</button></a> -->
						</div>
						<div class="col-lg-8 input">
							<div class="input-group">
				                <input type="show" class="form-control" oninput="load_students();" name="search" id="search" placeholder="Search Student...">
				                <span class="input-group-addon"><i class="fa fa-search"></i></span>
				            </div>
						</div>
					</div><hr>
					<div class="row">
						<div class="col-lg-12">
						<div class="table table-responsive">
							<table class="table table-hover table-sm table-striped table-bordered">
								<thead>
									<th>Student ID</th>
									<th>StudentName</th>
									<th>Grade Level</th>
									<th>Status</th>
									<th>School Year</th>
									<th>Action</th>
								</thead>
								<tbody id="load_students">
									
								</tbody>
							</table>
						</div>
					</div>
			</div>
					
</div>
<!-- Student -->
<div class="modal fade" id="student_form">
    <div class="modal-dialog modal-large">
	    <div class="modal-content">
	        <!-- Modal Header -->
	        <div class="modal-header">
	          <p class="title"><span class="fa fa-user-plus"></span> New Student</p>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <!-- Modal body -->
	        <div class="modal-body" autocomplete="on">
	        	<div class="container-fluid">
	        		<ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item ">
                        <a class="nav-link active" data-toggle="tab" href="#page1"><span class="fa fa-circle"></span> Page 1</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#page2"><span class="fa fa-circle"></span> Page 2</a>
                      </li>
                      <li class="nav-item col-lg-3">
                        	<div class="form-group">
					            <select id="school_year" class="form-control select_form">
					            	<option selected="" disabled="" value="0">Select School Year</option>
					            	<?php echo school_year($con); ?>
					            </select>
					         </div>
                      </li>
                      <li class="nav-item col-lg-3">
                        	<div class="form-group">
					            <select id="stud_type" class="form-control select_form">
					            	<option selected="" disabled="" value="none">Select Student Type</option>
					            	<option value="ELEMENTARY">Elementary</option>
					            	<option value="HIGHSCHOOL">Highschool</option>
					            </select>
					         </div>
                      </li>
                      <li class="nav-item col-lg-1">
                      	
                      </li>
                       <li class="nav-item">
                        	<button class="btn btn-success" onclick="save_update_student();"><span class="fa fa-save"></span> Save Student</button>
                        	<button class="btn btn-danger" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
                        
                      </li>
                    </ul>

                    <div class="row">
  					    <div class="col-md-12">
                          <div class="tab-content">
                          	<!-- Page 1 -->
                            <div id="page1" class="tab-pane active">
                              	<div class="row">
                              		<div class="col-lg-12">
                              			<div class="container-fluid">
	                              			<div class="card">
												<div class="card-header bg-info text-white">
													<span class="fa fa-user"></span> Student Information
												</div>
												<div class="card-body bg-light">
													<div class="col-lg-12 row">
														<div class="col-lg-3">
															<div class="form-group">
																<label for="ln" class="w3-small">Lastname:</label>
													            <input class="form-control" id="ln" name="ln" type="text" placeholder="Enter Lastname">
													         </div>
											     		</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="fn" class="w3-small">Firstname:</label>
													            <input class="form-control" id="fn" name="fn" type="text" placeholder="Enter Firstname">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="mn" class="w3-small">Middlename:</label>
													            <input class="form-control" id="mn" name="mn" type="text" placeholder="Enter Middlename">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="stud_id" class="w3-small">Student ID:</label>
													            <input class="form-control" disabled="" id="stud_id" name="stud_id" type="text" placeholder="Student ID">
													         </div>
														</div>
													</div>

													<div class="col-lg-12 row">
														<div class="col-lg-3">
															<div class="form-group">
																<label for="ln" class="w3-small">Gender:</label>
													            <select id="gender" class="form-control select_form">
													            	<option selected="" value="NONE">Select Gender</option>
													            	<option value="MALE">MALE</option>
													            	<option value="FEMALE">FEMALE</option>
													            </select>
													         </div>
											     		</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="birth_date" class="w3-small">Date of Birth:</label>
													            <input class="form-control" id="birth_date" name="birth_date" type="date" oninput="get_age();">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="age" class="w3-small">Age:</label>
													            <input class="form-control" disabled="" id="age" name="age" type="text" placeholder="Age">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="grade_level" class="w3-small">Grade Level:</label>
													            <select id="grade_level" class="form-control select_form">
													            	<?php echo get_grade_level($con); ?>
													            </select>
													         </div>
														</div>
													</div>

													<div class="col-lg-12 row">
														<div class="col-lg-5">
															<div class="form-group">
																<label for="stud_address" class="w3-small">Home Address:</label>
													            <input class="form-control" type="text" id="stud_address" name="stud_address" placeholder="Enter Full Address...">
													         </div>
											     		</div>
														<div class="col-lg-4">
															<div class="form-group">
																<label for="city" class="w3-small">City:</label>
													            <input class="form-control" id="city" name="city" type="text" placeholder="Enter City">
													         </div>
														</div>
														<div class="col-lg-3">
															<div class="form-group">
																<label for="language" class="w3-small">Language Spoken:</label>
													            <input class="form-control" id="language" name="language" type="text" placeholder="Enter Language Spoken">
													         </div>
														</div>
													</div>
													
												</div>
											</div>

											<div class="card">
												<div class="card-header bg-info text-white">
													<span class="fa fa-user"></span> Parent's Information
												</div>
												<div class="card-body bg-light">
													<div class="col-lg-12 row">
														<div class="col-lg-6">
															<div class="card">
																<div class="card-header bg-primary text-white">
																	Father's Information
																</div>
																<div class="card-body">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label for="father_name" class="w3-small">Father's Name:</label>
																            <input class="form-control" id="father_name" name="father_name" type="text" placeholder="Enter Father's Name">
																         </div>
																         <div class="form-group">
																			<label for="f_religion" class="w3-small">Religion:</label>
																            <input class="form-control" id="f_religion" name="f_religion" type="text" placeholder="Enter Religion">
																         </div>
																         <div class="form-group">
																			<label for="f_nationality" class="w3-small">Nationality:</label>
																            <input class="form-control" id="f_nationality" name="f_nationality" type="text" placeholder="Enter Nationality">
																         </div>
																         <div class="form-group">
																			<label for="f_educ_attain" class="w3-small">Educational Attainment:</label>
																            <input class="form-control" id="f_educ_attain" name="f_educ_attain" type="text" placeholder="Enter Educational Attainment">
																         </div>
																         <div class="form-group">
																			<label for="f_occupation" class="w3-small">Occupation:</label>
																            <input class="form-control" id="f_occupation" name="f_occupation" type="text" placeholder="Enter Occupation">
																         </div>
																         <div class="form-group">
																			<label for="f_contact" class="w3-small">Telephone/Cp No.:</label>
																            <input class="form-control" id="f_contact" name="f_contact" type="text" placeholder="Enter Contact Number">
																         </div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-6">
															<div class="card">
																<div class="card-header bg-primary text-white">
																	Mother's Information
																</div>
																<div class="card-body">
																	<div class="col-sm-12">
																		<div class="form-group">
																			<label for="mother_name" class="w3-small">Mother's Name:</label>
																            <input class="form-control" id="mother_name" name="mother_name" type="text" placeholder="Enter Mother's Name">
																         </div>
																         <div class="form-group">
																			<label for="m_religion" class="w3-small">Religion:</label>
																            <input class="form-control" id="m_religion" name="m_religion" type="text" placeholder="Enter Religion">
																         </div>
																         <div class="form-group">
																			<label for="m_nationality" class="w3-small">Nationality:</label>
																            <input class="form-control" id="m_nationality" name="m_nationality" type="text" placeholder="Enter Nationality">
																         </div>
																         <div class="form-group">
																			<label for="m_educ_attain" class="w3-small">Educational Attainment:</label>
																            <input class="form-control" id="m_educ_attain" name="m_educ_attain" type="text" placeholder="Enter Educational Attainment">
																         </div>
																         <div class="form-group">
																			<label for="m_occupation" class="w3-small">Occupation:</label>
																            <input class="form-control" id="m_occupation" name="m_occupation" type="text" placeholder="Enter Occupation">
																         </div>
																         <div class="form-group">
																			<label for="m_contact" class="w3-small">Telephone/Cp No.:</label>
																            <input class="form-control" id="m_contact" name="m_contact" type="text" placeholder="Enter Contact Number">
																         </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
                              			</div>
                              		</div>
                              	</div> 
                            </div>
                            <!-- Page 1 -->

                            <!-- Page 2 -->
                            <div id="page2" class="tab-pane">
                            	<div class="row">
                            		<div class="col-lg-12">
                            			<div class="container-fluid">
                            				<div class="card">
                            					<div class="card-header bg-info text-white">
                            						<span class="fa fa-info"></span> Other Information
                            					</div>
                            					<div class="card-body">
                            						<div class="col-lg-12 row">
                            							<div class="col-lg-7 row">
                            								<div class="col-lg-6">
	                            								<div class="form-group">
																	<label for="ln" class="w3-small">Student's Lives with(Select One):
																	</label>
														            <select id="lives_with" class="form-control select_form">
														            	<option selected="" value="NONE">Select One</option>
														            	<option value="Both Parent">Both Parent</option>
														            	<option value="Mother Only">Mother Only</option>
														            	<option value="Father Only">Father Only</option>
														            </select>
														         </div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="lives_with_other" class="w3-small">Some Other Than Parent:</label>
														            <input class="form-control" id="lives_with_other" name="lives_with_other" type="text" placeholder="Enter Some Other Than Parent">
													         	</div>
                            								</div>
                            							</div>
                            							<div class="col-lg-5">
                            								<div class="form-group">
																<label for="recommend_by" class="w3-small">If the Student is new to Galilee Integraded School recommended by?:</label>
													            <input class="form-control" id="recommend_by" name="recommend_by" type="text" placeholder="Enter Recommended by.">
												         	</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12 row">
                            							<div class="col-lg-6">
                        									<div class="form-group">
																<label for="prev_school" class="w3-small">Previous School Attended:</label>
													            <input class="form-control" id="prev_school" name="prev_school" type="text" placeholder="Enter School">
												         	</div>
                        								</div>
                        								<div class="col-lg-6">
                        									<div class="form-group">
																<label for="prev_school_city" class="w3-small">City:</label>
													            <input class="form-control" id="prev_school_city" name="prev_school_city" type="text" placeholder="Enter City">
												         	</div>
                        								</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Sibling Attending Galilee Integrated School in the same School Year:</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling1_name" class="w3-small">Name:</label>
														            <input class="form-control" id="sibling1_name" name="sibling1_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling1_grade" class="w3-small">Grade:
																	</label>
														            <select id="sibling1_grade" class="form-control select_form">
														            	<?php echo get_grade_level($con); ?>
														            </select>
														         </div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling2_name" class="w3-small">Name:</label>
														            <input class="form-control" id="sibling2_name" name="sibling2_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="sibling2_grade" class="w3-small">Grade:
																	</label>
														            <select id="sibling2_grade" class="form-control select_form">
														            	<?php echo get_grade_level($con); ?>
														            </select>
														         </div>
                            								</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Parent/Guardian:</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="guardian_name" class="w3-small">Name:</label>
														            <input class="form-control" id="guardian_name" name="guardian_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_relation" class="w3-small">Relationship:</label>
														            <input class="form-control" id="g_relation" name="g_relation" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_church_affil" class="w3-small">Church Affiliation:</label>
														            <input class="form-control" id="g_church_affil" name="g_church_affil" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="g_occupation" class="w3-small">Occupation:</label>
														            <input class="form-control" id="g_occupation" name="g_occupation" type="text" placeholder="Enter Occupation">
													         	</div>
                            								</div>
                            								<div class="col-lg-6">
                            									<div class="form-group">
																	<label for="g_company" class="w3-small">Company Name:
																	</label>
														            <input class="form-control" id="g_company" name="g_company" type="text" placeholder="Enter Company">
														         </div>
                            								</div>
                            							</div>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_contact" class="w3-small">Cellphone:</label>
														            <input class="form-control" id="g_contact" name="g_contact" type="text" placeholder="Enter Cellphone No.">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_phoneno" class="w3-small">Work Phone:</label>
														            <input class="form-control" id="g_phoneno" name="g_phoneno" type="text" placeholder="Enter Relation">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="g_email" class="w3-small">Email</label>
														            <input class="form-control" id="g_email" name="g_email" type="text" placeholder="Enter Email">
													         	</div>
                            								</div>
                            							</div>
                            						
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>In the case of an emergency when parents/guardian cannot be reached, Contact::</b></label>
                            							<div class="col-lg-12 row">
                            								<div class="col-lg-3">
                            									<div class="form-group">
																	<label for="emergency_name" class="w3-small">Name:</label>
														            <input class="form-control" id="emergency_name" name="emergency_name" type="text" placeholder="Enter Name">
													         	</div>
                            								</div>
                            								<div class="col-lg-3">
                            									<div class="form-group">
																	<label for="e_relation" class="w3-small">Relationship:</label>
														            <input class="form-control" id="e_relation" name="e_relation" type="text" placeholder="Enter Relationship">
													         	</div>
                            								</div>
                            								<div class="col-lg-2">
                            									<div class="form-group">
																	<label for="e_contact" class="w3-small">Contact:</label>
														            <input class="form-control" id="e_contact" name="e_contact" type="text" placeholder="Enter Contact">
													         	</div>
                            								</div>
                            								<div class="col-lg-4">
                            									<div class="form-group">
																	<label for="e_address" class="w3-small">Address:</label>
														            <input class="form-control" id="e_address" name="e_address" type="text" placeholder="Enter Address">
													         	</div>
                            								</div>
                            							</div>
                            						</div>
                            						<div class="col-lg-12">
                            							<label class="text-info"><b>Please Check if the student has the following sickness:</b></label>
                            							<input type="hidden" id="sickness">
                            							<div class="col-lg-12 row" id="load_sickness">
                            								
                            							</div>
                            						</div>
                            					</div>
                            				</div>
                            			</div>
                            		</div>
                            	</div>
                            </div>
                            <!-- Page 2 -->
                          </div>

  					     </div>
					</div>

	        	</div>
	        </div>
	    </div>
	</div>
</div>
</div>
<!-- Enroll Student -->
<div class="modal fade" id="enroll_student">
	<div class="modal-dialog modal-medium">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-user-plus"></span> Enroll Student</p>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="container-fluid">
					<div class="col-lg-12 row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="enroll_id" class="w3-small">Enrollment ID:</label>
					            <input class="form-control" id="enroll_id" name="enroll_id" type="text" placeholder="Enrollment ID" disabled="">
					         </div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="student_type" class="w3-small">Student Type:</label>
					            <input class="form-control" id="student_type" name="student_type" type="text" placeholder="Enrollment ID" disabled="">
					         </div>
						</div>
					</div>
					<div class="col-lg-12 row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="student_id" class="w3-small">Student ID:</label>
					            <input class="form-control" id="student_id" name="student_id" type="text" placeholder="Enrollment ID" disabled="">
					         </div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="student_grade" class="w3-small">Grade Level:</label>
					            <input class="form-control" id="student_grade" name="student_grade" type="text" placeholder="Enrollment ID" disabled="">
					         </div>
						</div>
					</div>
					<div class="col-lg-12 row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="student_name" class="w3-small">Student Name:</label>
					            <input class="form-control" id="student_name" name="student_name" type="text" placeholder="Enrollment ID" disabled="">
					         </div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="section_id" class="w3-small">Select Section:</label>
								<span id="load_sections">
									
								</span>
					            
					         </div>
						</div>
					</div>
				</div>
				<input type="hidden" id="grade_id">
				<div class="table table-responsive-sm">
					<table class="table table-hover table-sm table-striped table-bordered">
						<thead>
							<tr>
								<th>Subject Code</th>
								<th>Description</th>
								<th>Time</th>
								<th>Days</th>
							</tr>
						</thead>
						<tbody id="section_details">
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer none" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<form action="print_student.php" method="post">
					<input type="text" style="display: none" id="print_id" name="print_id" autocomplete="off"/>
					<button class="btn btn-dark" onclick="btn_enroll();" type="submit" id="btn-print><span class="fa fa-print"></span> Enroll Student
					</button>  
				</form>
			</div>
		</div>
	</div>
</div>
<?php include('modal.php'); ?>
<!-- Student -->
<div class="modal fade" id="generate_students">
    <div class="modal-dialog modal-large">
	    <div class="modal-content">
	        <!-- Modal Header -->
	        <div class="modal-header">
	          <p class="title"><span class="fa fa-user-plus"></span> Generate Old Student</p>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <!-- Modal body -->
	        <div class="modal-body" autocomplete="on">
	        	<div class="container-fluid">
	        			<div class="form-group">
	        				<button class="btn btn-default" disabled="" id="btn-update">Update</button>	        			
	        			 </div>
	        		<div class="row">
		        		<div class="col-lg-12">
							<div class="table table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th></th>
											<th>Student ID</th>
											<th>Name</th>
											<th>Grade</th>
										</tr>
									</thead>
									<tbody id="load_old_students">
										
									</tbody>
								</table>
							</div>
						</div>
	        		</div>
	        	</div>
	        	
	        </div>
	    </div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
 
 //Approve ALl
 $('#btn-update').click(function(){
  
 if(confirm("Are you sure you want to Update Students?"))
  {
  	var id = [];
   $(':checkbox:checked').each(function(i){
    id[i] = $(this).val();
   });
   
   if(id.length === 0) //tell you if the array is empty
   {
    alert("Please Select Atleast one Student");
   }
   else
   {
    $.ajax({
     url:'update_old_student.php',
     method:'POST',
     data:{id:id},
     cache:false,
     success:function(data)
     {
     	if(data == 404){
     		alert('Error on updating Students!');
			
     	}else{
			alert('Student successfully updated');
			$('#generate_students').modal('hide');
			 document.getElementById('btn-update').disabled= true;
			 load_students();
     	}
      
     }
     
    });
   }
  }
   


 });

 });
</script>

</body>
</html>