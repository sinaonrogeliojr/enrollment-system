<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search']);

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id, t3.`id` as sy FROM tbl_enrollment_form t1 
	LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
	LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
	LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Not Enrolled' and old_student = 'NEW';");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['stud_id']; ?></td>
				<td><?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn']; ?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td>
					<?php 

						$status='';
						if($row['status'] == 'Enrolled'){
							$status = '<span class="badge badge-success">Enrolled</span>';
						}else{
							$status = '<span class="badge badge-danger">Not Enrolled</span>';
						}
						echo $status;
					?>

				</td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<button class="btn btn-sm btn-success" onclick="enroll_student('<?php echo $row['id'] ?>','<?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn'];?>','<?php echo $row['grade_level']?>','<?php echo $row['stud_type'] ?>','<?php echo $row['grade_id'] ?>')"title="Enroll <?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn'];?>">
						<span class="fa fa-sign-in"></span> Enroll
					</button>
					<button class="btn btn-sm btn-default" onclick="update_student('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['birth_date'] ?>','<?php echo $row['age'] ?>','<?php echo $row['stud_address'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['grade_level'] ?>','<?php echo $row['language'] ?>','<?php echo $row['city'] ?>','<?php echo $row['father_name'] ?>','<?php echo $row['f_religion'] ?>','<?php echo $row['f_nationality'] ?>','<?php echo $row['f_educ_attain'] ?>','<?php echo $row['f_occupation'] ?>','<?php echo $row['f_contact'] ?>','<?php echo $row['mother_name'] ?>','<?php echo $row['m_religion'] ?>','<?php echo $row['m_nationality'] ?>','<?php echo $row['m_educ_attain'] ?>','<?php echo $row['m_occupation'] ?>','<?php echo $row['m_contact'] ?>','<?php echo $row['lives_with'] ?>','<?php echo $row['lives_with_other'] ?>','<?php echo $row['recommend_by'] ?>','<?php echo $row['prev_school'] ?>','<?php echo $row['prev_school_city'] ?>','<?php echo $row['sibling1_name'] ?>','<?php echo $row['sibling1_grade'] ?>','<?php echo $row['sibling2_name'] ?>','<?php echo $row['sibling2_grade'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['g_occupation'] ?>','<?php echo $row['g_contact'] ?>','<?php echo $row['g_relation'] ?>','<?php echo $row['g_church_affil'] ?>','<?php echo $row['g_company'] ?>','<?php echo $row['g_phoneno'] ?>','<?php echo $row['g_email'] ?>','<?php echo $row['emergency_name'] ?>','<?php echo $row['e_relation'] ?>','<?php echo $row['e_contact'] ?>','<?php echo $row['e_address'] ?>','<?php echo $row['sickness'] ?>','<?php echo $row['sy'] ?>','<?php echo $row['stud_type'] ?>')"><span class="fa fa-edit"></span> Update</button>
					<button class="btn btn-sm btn-danger" onclick="delete_student('<?php echo $row['id'] ?>')"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Student Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t3.`school_year` FROM tbl_enrollment_form t1 
	LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
	LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`status` = 'Not Enrolled' and concat(ln,fn,mn,section,stud_id) like '%$search%';");


	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr>
				<td><?php echo $row['stud_id']; ?></td>
				<td><?php echo $row['ln'].' '. $row['fn'].' '. $row['mn']; ?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td>
					<?php 
						$status='';
						if($row['status'] == 1){
							$status = '<span class="badge badge-success">Enrolled</span>';
						}else{
							$status = '<span class="badge badge-danger">Not Enrolled</span>';
						}
						echo $status;
					?>

				</td>
				<td><?php echo $row['school_year']; ?></td>
				<td>
					<button class="btn btn-sm btn-success" onclick="enroll_student('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn'];?>','<?php echo $row['grade_level']?>','<?php echo $row['stud_type'] ?>','<?php echo $row['grade_id'] ?>')"title="Enroll <?php echo $row['ln'].',  '. $row['fn'].' '. $row['mn'];?>">
						<span class="fa fa-sign-in"></span> Enroll
					</button>
					<button class="btn btn-sm btn-default" onclick="update_student('<?php echo $row['stud_id'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['birth_date'] ?>','<?php echo $row['age'] ?>','<?php echo $row['stud_address'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['grade_level'] ?>','<?php echo $row['language'] ?>','<?php echo $row['city'] ?>','<?php echo $row['father_name'] ?>','<?php echo $row['f_religion'] ?>','<?php echo $row['f_nationality'] ?>','<?php echo $row['f_educ_attain'] ?>','<?php echo $row['f_occupation'] ?>','<?php echo $row['f_contact'] ?>','<?php echo $row['mother_name'] ?>','<?php echo $row['m_religion'] ?>','<?php echo $row['m_nationality'] ?>','<?php echo $row['m_educ_attain'] ?>','<?php echo $row['m_occupation'] ?>','<?php echo $row['m_contact'] ?>','<?php echo $row['lives_with'] ?>','<?php echo $row['lives_with_other'] ?>','<?php echo $row['recommend_by'] ?>','<?php echo $row['prev_school'] ?>','<?php echo $row['prev_school_city'] ?>','<?php echo $row['sibling1_name'] ?>','<?php echo $row['sibling1_grade'] ?>','<?php echo $row['sibling2_name'] ?>','<?php echo $row['sibling2_grade'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['g_occupation'] ?>','<?php echo $row['g_contact'] ?>','<?php echo $row['g_relation'] ?>','<?php echo $row['g_church_affil'] ?>','<?php echo $row['g_company'] ?>','<?php echo $row['g_phoneno'] ?>','<?php echo $row['g_email'] ?>','<?php echo $row['emergency_name'] ?>','<?php echo $row['e_relation'] ?>','<?php echo $row['e_contact'] ?>','<?php echo $row['e_address'] ?>','<?php echo $row['sickness'] ?>','<?php echo $row['sy'] ?>','<?php echo $row['stud_type'] ?>',false)"><span class="fa fa-edit"></span> Update</button>
					<button class="btn btn-sm btn-danger" onclick="delete_student('<?php echo $row['stud_id'] ?>',false)"><span class="fa fa-trash"></span> Delete</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="6">No Student Found!</td>';
   }

}

?>