<?php 
session_start();
if (!isset($_SESSION['registrar'])) {
	@header('location:../');
}

include('../db_config.php');
function get_grade_level($con){
    $sql = mysqli_query($con,"SELECT * from tbl_grade_level");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['grade_level'].'</option>';
        }
    }
}
function school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by status ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['id'].'">'.$row['school_year'].'</option>';
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registrar</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="registrar.js"></script>
</head>
<body onload="load_enrolled_students();">
<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System</strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu " onclick="window.location='enrollment.php'"><span class="fa fa-sign-in fa-2x"></span> Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu " onclick="window.location='manage_enrollment.php'"><span class="fa fa-edit fa-2x"></span> Manage Enrollment</button>
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='studentlist.php'"><span class="fa fa-users fa-2x"></span> Student List</button>
					<!--<button class="btn btn-lg btn-success btn-menu"><span class="fa fa-gears fa-2x"></span> Admin Options</button> -->
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	
	<div class="row">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Student Masterlist</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
			            <select id="f_sy" class="form-control select_form" oninput="clear_dropdown();load_enrolled_students(); get_print_values();">
			            	<option value="0" selected="" disabled="">--Select School Year--</option>
			            	<?php echo school_year($con); ?>
			            </select>
			         </div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
			            <select id="f_grade" class="form-control select_form" oninput="load_dynamic_section(); load_enrolled_students(); get_print_values();">
			            	<option value="0" selected="" disabled="">--Select Grade Level--</option>
			            	<?php echo get_grade_level($con); ?>
			            </select>
			         </div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
			            <select id="f_section" class="form-control select_form" oninput="load_enrolled_students(); get_print_values();">
			            	
			            </select>
			         </div>
				</div>
				<div class="col-lg-3 input">
					<div class="input-group">
		                <input type="show" class="form-control" oninput="load_enrolled_students();" name="f_search" id="f_search" placeholder="Search Student...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div>
				<div class="col-lg-2 input">
					<div class="btn btn-group btn-group-justified" >

						<form action="print_all.php" method="post">
							<input type="text" style="display: none" id="p_sy3" name="p_sy3" autocomplete="off"/>
							<button class="btn btn-dark btn-sm" type="submit" disabled="" id="btn_print_sy"><span class="fa fa-print"></span> Print All
							</button>  
						</form>

						<form action="print_student_list.php" method="post">
							<input type="text" style="display: none" id="p_sy" name="p_sy" autocomplete="off"/>
							<input type="text" style="display: none" id="p_grade" name="p_grade" autocomplete="off"/>
							<input type="text" style="display: none" id="p_section" name="p_section" autocomplete="off"/>
							<button class="btn btn-dark btn-sm" type="submit" disabled="" id="btn_print_all"><span class="fa fa-print"></span> Print Section
							</button>  
						</form>

						<form action="print_grade_only.php" method="post">
							<input type="text" style="display: none" id="p_sy2" name="p_sy2" autocomplete="off"/>
							<input type="text" style="display: none" id="p_grade2" name="p_grade2" autocomplete="off"/>
							<button class="btn btn-dark btn-sm" type="submit" disabled="" id="btn_print_grade"><span class="fa fa-print"></span> Print Grade
							</button>  
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="table table-responsive">
			<table class="table table-hover table-sm table-striped table-bordered">
				<thead>
					<th>Student ID</th>
					<th>StudentName</th>
					<th>Grade</th>
					<th>Section</th>
					<th>School Year</th>
					<th>Action</th>
				</thead>
				<tbody id="load_enrolled_students">
					
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Student Information-->
<div class="modal fade" id="student_info">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-user-plus"></span> Student Info: <strong id="student_name"></strong></p>
				<input type="hidden" id="s_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body" id="display_info">
				
			</div>
			<div class="modal-footer none" id="buttons">
				<button class="btn btn-default btn-block" data-dismiss="modal"><span class="fa fa-times"></span> Close</button>
			</div>
		</div>
	</div>
</div>
<?php include('modal.php'); ?>
</body>
</html>