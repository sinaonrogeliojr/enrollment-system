<?php 
include('../db_config.php');
$stud_id = $_POST['print_id'];
$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t2.`id` as sec_id,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`id` = '$stud_id'");
$row = mysqli_fetch_assoc($sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Student</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/print.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="registrar.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="col-lg-12 text-center">
		<img src="../img/logo.jpg" class="img-responsive" width="90px" height="90px">
		<h4>Galilee Integraded School</h4>
		<hr>
		<div class="col-lg-12 row">
			<div class="col-lg-1"></div>
			<div class="col-lg-7">
				<table>
					<tr>
						<td class="text-left">Student ID: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['stud_id']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">Student Name: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['ln'].', '. $row['fn'].' '. $row['mn']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">Grade: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['grade_level']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">Section: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['section']; ?></b></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4 float-right">
				<table>
					<tr>
						<td class="text-right">Date Print: &nbsp;</td>
						<td class="text-left"><b><?php echo date('Y-m-d'); ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Status: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['status']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Date Enrolled: &nbsp;</td>
						<td class="text-left"><b><?php echo date('Y-m-d',strtotime($row['date_enrolled'])); ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Address: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['stud_address']; ?></b></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="col-lg-12">
			<table class="table table-sm">
				<thead>
					<tr>
						<th>Subject Code</th>
						<th>Descriptive Title</th>
						<th>Time</th>
						<th>Day</th> 	
					</tr>
				</thead>
				<tbody>
					<?php 
						$sec = mysqli_query($con, "SELECT t1.*,t2.`section`,t2.`id` as sec_id,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id FROM tbl_enrollment_form t1 
									LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
									LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
									LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`stud_id` = '$stud_id'");
						$row_sec = mysqli_fetch_assoc($sec);

						$sec_id = $row_sec['sec_id'];
						$query = mysqli_query($con, "SELECT t1.*,t2.`subject_code`,t2.`subject_name` FROM tbl_section_content t1 LEFT JOIN tbl_subjects t2 ON t1.`subjects` = t2.`id` WHERE t1.`section_id` = '$sec_id'");
						while ($rows = mysqli_fetch_assoc($query)) { ?>

							<tr>
								<td><?php echo $rows['subject_code'] ?></td>
								<td><?php echo $rows['subject_name'] ?></td>
								<td><?php echo $rows['time'] ?></td>
								<td><?php echo $rows['day'] ?></td>
							</tr>

						<?php
						}

					?>
				</tbody>
			</table>
		</div>
		<hr><div class="page-break"></div>
		<div class="col-lg-12 row">
			<?php 
				$grade_id = $row['grade_id'];
				$query = mysqli_query($con, "SELECT * FROM tbl_billings WHERE grade_id = '$grade_id'");
				$data = mysqli_fetch_assoc($query);			
			?>
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<table>
					<tr>
						<td class="text-right">Tuition Fee: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['tuition_fee']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Registration Fee: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['reg_fee']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Miscellaneus Fee: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['misc_fee']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Laboratory Fees: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['computer']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Aircon: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['aircon']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Books: &nbsp;</td>
						<td class="text-left"><b><?php echo $data['books']; ?></b></td>
					</tr>
					<tr>
						<td class="text-right" style="border-top: 1px solid red; ">Total: &nbsp;</td>
						<td class="text-left" style="border-top: 1px solid red; "><b><?php echo $data['total']; ?></b></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4"></div>
		</div>
	</div>
	<br><br>
	<div class="col-lg-12 row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4 text-center" style="line-height: 15px;">
				<hr style="border: 1px solid #000;">
				Claudine Videz<br>
				Registrar
			</div>
		</div><br><br>
		<div class="col-lg-12 text-center" id="btns">
			<button class="btn btn-sm btn-default" onclick="print_preview();"><span class="fa fa-print"></span> Preview</button>
			<button class="btn btn-sm btn-default" onclick="window.location='enrollment.php'"> Back</button>
		</div>
		<br>
</div>
<script type="text/javascript">
	function print_preview(){
		$('#btns').addClass('none');
		window.print();
		$('#btns').removeClass('none');
	}
</script>
<style type="text/css">
	@media print {
		.page-break {
        page-break-before: always;  
   		}
	}
</style>
</body>
</html>