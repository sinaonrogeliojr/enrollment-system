function load_old_students(){
  $.ajax({
  type:"POST",
  url:"load_old_students.php",
  cache:false,
  beforeSend:function(){$("#load_old_students").html('<td colspan="5"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_old_students").html(data);},800); 
  }
  }); 
}

function load_students(){
  var search = document.getElementById('search');
  var mydata = 'search=' + search.value;

  $.ajax({
  type:"POST",
  url:"load_students.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_students").html('<td colspan="6"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_students").html(data);},800); 
  }
  }); 
}

function load_sickness(){
  var sickness = document.getElementById('sickness');
  var mydata = 'sickness=' + sickness.value;

  $.ajax({
  type:"POST",
  url:"load_sickness.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_sickness").html('<td colspan="6"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_sickness").html(data);},800); 
  }
  }); 
}

function get_sick_value(){
  var chkArray = [];
  
  /* look for all checkboes that have a parent id called 'checkboxlist' attached to it and check if it was checked */
  $("#load_sickness input:checked").each(function() {
    chkArray.push($(this).val());
  });
  
  /* we join the array separated by the comma */
  var selected;
  selected = chkArray.join(',' + ' ') ;

  $("#sickness").val(selected);
}

function generate_id(){
  $.ajax({
  type:"POST",
  url:"generate_id.php",
  cache:false,
  success:function(data){
  	$('#stud_id').val(data);
  }
  }); 
}

function save_update_student(){
  var is_enroll,stud_id,ln,fn,mn,birth_date,age,stud_address,gender,grade_level,language,city,father_name,f_religion,f_nationality,f_educ_attain,f_occupation,f_contact,mother_name,m_religion,m_nationality,m_educ_attain,m_occupation,m_contact,lives_with,recommend_by,prev_school,prev_school_city,sibling1_name,sibling1_grade,sibling2_name,sibling2_grade,guardian_name,g_occupation,g_contact,g_relation,g_church_affil,g_company,g_phoneno,g_email,emergency_name,e_relation,e_contact,e_address,sickness,school_year,stud_type;

  stud_id = $('#stud_id').val();
  ln = $('#ln').val();
  fn = $('#fn').val();
  mn = $('#mn').val();
  birth_date = $('#birth_date').val();
  age = $('#age').val();
  stud_address = $('#stud_address').val();
  gender = $('#gender').val();
  grade_level = $('#grade_level').val();
  language = $('#language').val();
  city = $('#city').val();
  father_name = $('#father_name').val();
  f_religion = $('#f_religion').val();
  f_nationality = $('#f_nationality').val();
  f_educ_attain = $('#f_educ_attain').val();
  f_occupation = $('#f_occupation').val();
  f_contact = $('#f_contact').val();
  mother_name = $('#mother_name').val();
  m_religion = $('#m_religion').val();
  m_nationality = $('#m_nationality').val();
  m_educ_attain = $('#m_educ_attain').val();
  m_occupation = $('#m_occupation').val();
  m_contact = $('#m_contact').val();
  lives_with = $('#lives_with').val();
  lives_with_other = $('#lives_with_other').val();
  recommend_by = $('#recommend_by').val();
  prev_school = $('#prev_school').val();
  prev_school_city = $('#prev_school_city').val();
  sibling1_name = $('#sibling1_name').val();
  sibling1_grade = $('#sibling1_grade').val();
  sibling2_name = $('#sibling2_name').val();
  sibling2_grade = $('#sibling2_grade').val();
  guardian_name = $('#guardian_name').val();
  g_occupation = $('#g_occupation').val();
  g_contact = $('#g_contact').val();
  g_relation = $('#g_relation').val();
  g_church_affil = $('#g_church_affil').val();
  g_company = $('#g_company').val();
  g_phoneno = $('#g_phoneno').val();
  g_email = $('#g_email').val();
  emergency_name = $('#emergency_name').val();
  e_relation = $('#e_relation').val();
  e_contact = $('#e_contact').val();
  e_address = $('#e_address').val();
  sickness = $('#sickness').val();
  school_year = $('#school_year').val();
  stud_type = $('#stud_type').val();
  is_enroll = $('#is_enroll').val();

  if(school_year == null){
    alert("Please Select School Year");
    $('#school_year').focus();
  }
  else if(stud_type == null){
    alert("Please Select Student Type");
    $('#stud_type').focus();
  }
  else if(ln == ''){
    alert("Please Input Lastname");
    $('#ln').focus();
  }
  else if(fn == ''){
    alert("Please Input Firstname");
    $('#fn').focus();
  }
  else if(gender == 'NONE'){
    alert("Please Select gender");
    $('#gender').focus();
  }
  else if(birth_date == ''){
    alert("Please Input Birth date");
    $('#birth_date').focus();
  }
  else if(stud_address == ''){
    alert("Please Input Address");
    $('#stud_address').focus();
  }
  else if(city == ''){
    alert("Please Input City");
    $('#city').focus();
  }
  else if(language == ''){
    alert("Please Input language Spoken");
    $('#language').focus();
  }
  else{
    var mydata = 'stud_id=' + stud_id + '&ln=' + ln + '&fn=' + fn + '&mn=' + mn + '&birth_date=' + birth_date + '&age=' + age +'&stud_address=' + stud_address + '&gender=' + gender + '&grade_level=' + grade_level + '&language=' + language + '&city=' + city + '&father_name=' + father_name + '&f_religion=' + f_religion + '&f_nationality=' + f_nationality + '&f_educ_attain=' + f_educ_attain + '&f_occupation=' + f_occupation + '&f_contact=' + f_contact + '&mother_name=' + mother_name + '&m_religion=' + m_religion + '&m_nationality=' + m_nationality + '&m_educ_attain=' + m_educ_attain + '&m_occupation=' + m_occupation + '&m_contact=' + m_contact + '&lives_with=' + lives_with + '&lives_with_other=' + lives_with_other + '&recommend_by=' + recommend_by + '&prev_school=' + prev_school + '&prev_school_city=' + prev_school_city + '&sibling1_name=' + sibling1_name + '&sibling1_grade=' + sibling1_grade + '&sibling2_name=' + sibling2_name + '&sibling2_grade=' + sibling2_grade + '&guardian_name=' + guardian_name + '&g_occupation=' + g_occupation + '&g_contact=' + g_contact + '&g_relation=' + g_relation + '&g_church_affil=' + g_church_affil + '&g_company=' + g_company + '&g_phoneno=' + g_phoneno + '&g_email=' + g_email + '&emergency_name=' + emergency_name + '&e_relation=' + e_relation + '&e_contact=' + e_contact +  '&e_address=' + e_address + '&sickness=' + sickness + '&school_year=' + school_year + '&stud_type=' + stud_type; 
    $.ajax({
    type:"POST",
    url:"save_student.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      alert('New Student Successfully Saved.');
      clear_form();
      load_students();
    }
    else if(data == 2){
      //alert(is_enroll);

      if(is_enroll === 'true'){
        $('#student_form').modal('hide');
        alert('Student Successfully Updated.');
        clear_form();
        load_enrolled_students();
      }else{
        $('#student_form').modal('hide');
        alert('Student Successfully Updated.');
        clear_form();
        load_students();
      }
      
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

//disable inputs
function disable_inputs(){
  var is_enroll = $('#is_enroll').val();
  //alert(is_enroll);
  if(is_enroll === 'true'){
   document.getElementById('school_year').disabled = true;
   document.getElementById('stud_type').disabled = true;
   document.getElementById('grade_level').disabled = true;
  }else{

  }

}

function delete_student(id,is_enroll){
  //alert(is_enroll);
  var mydata = 'id=' + id;
  if(confirm("Are you sure you want to delete this?"))
  {
     $.ajax({
    type:"POST",
    url:"delete_student.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      if(is_enroll == 'true'){
        alert('Student Successfully Deleted.');
        load_enrolled_students();
      }else{
        alert('Student Successfully Deleted.');
        load_students();
        generate_id();
      }
    }
    else if(data == 404){
      alert('Student is not deletable because it has related data!')
    }
    else 
    {
    alert(data);
    }
    }
    });
  }
}

function clear_form(){
  $('#ln').val('');
  $('#fn').val('');
  $('#mn').val('');
  $('#birth_date').val('');
  $('#age').val('');
  $('#stud_address').val('');
  $('#language').val('');
  $('#city').val('');
  $('#father_name').val('');
  $('#f_religion').val('');
  $('#f_nationality').val('');
  $('#f_educ_attain').val('');
  $('#f_occupation').val('');
  $('#f_contact').val('');
  $('#mother_name').val('');
  $('#m_religion').val('');
  $('#m_nationality').val('');
  $('#m_educ_attain').val('');
  $('#m_occupation').val('');
  $('#m_contact').val('');
  $('#lives_with_other').val('');
  $('#recommend_by').val('');
  $('#prev_school').val('');
  $('#prev_school_city').val('');
  $('#sibling1_name').val('');
  $('#sibling2_name').val('');
  $('#guardian_name').val('');
  $('#g_occupation').val('');
  $('#g_contact').val('');
  $('#g_relation').val('');
  $('#g_church_affil').val('');
  $('#g_company').val('');
  $('#g_phoneno').val('');
  $('#g_email').val('');
  $('#emergency_name').val('');
  $('#e_relation').val('');
  $('#e_contact').val('');
  $('#e_address').val('');
  $('#sickness').val('');
  $('#is_enroll').val('');
  generate_id();

}

function update_student(stud_id,ln,fn,mn,birth_date,age,stud_address,gender,grade_level,language,city,father_name,f_religion,f_nationality,f_educ_attain,f_occupation,f_contact,mother_name,m_religion,m_nationality,m_educ_attain,m_occupation,m_contact,lives_with,lives_with_other,recommend_by,prev_school,prev_school_city,sibling1_name,sibling1_grade,sibling2_name,sibling2_grade,guardian_name,g_occupation,g_contact,g_relation,g_church_affil,g_company,g_phoneno,g_email,emergency_name,e_relation,e_contact,e_address,sickness,school_year,stud_type,is_enroll){
  
  $('#school_year').val(school_year);
  $('#stud_type').val(stud_type);
  $('#stud_id').val(stud_id);
  $('#ln').val(ln);
  $('#fn').val(fn);
  $('#mn').val(mn);
  $('#birth_date').val(birth_date);
  $('#age').val(age);
  $('#stud_address').val(stud_address);
  $('#gender').val(gender);
  $('#language').val(language);
  $('#city').val(city);
  $('#father_name').val(father_name);
  $('#f_religion').val(f_religion);
  $('#f_nationality').val(f_nationality);
  $('#f_educ_attain').val(f_educ_attain);
  $('#f_occupation').val(f_occupation);
  $('#f_contact').val(f_contact);
  $('#mother_name').val(mother_name);
  $('#m_religion').val(m_religion);
  $('#m_nationality').val(m_nationality);
  $('#m_educ_attain').val(m_educ_attain);
  $('#m_occupation').val(m_occupation);
  $('#m_contact').val(m_contact);
  $('#lives_with_other').val(lives_with_other);
  $('#recommend_by').val(recommend_by);
  $('#prev_school').val(prev_school);
  $('#prev_school_city').val(prev_school_city);
  $('#sibling1_name').val(sibling1_name);
  $('#sibling2_name').val(sibling2_name);
  $('#guardian_name').val(guardian_name);
  $('#g_occupation').val(g_occupation);
  $('#g_contact').val(g_contact);
  $('#g_relation').val(g_relation);
  $('#g_church_affil').val(g_church_affil);
  $('#g_company').val(g_company);
  $('#g_phoneno').val(g_phoneno);
  $('#g_email').val(g_email);
  $('#emergency_name').val(emergency_name);
  $('#e_relation').val(e_relation);
  $('#e_contact').val(e_contact);
  $('#e_address').val(e_address);
  $('#sickness').val(sickness);
  $('#is_enroll').val(is_enroll);
  load_sickness();
  $('#student_form').modal('show');
  disable_inputs();
}

function enroll_student(stud_id,name,grade,stud_type,grade_id){
  $("#student_id").val(stud_id);
  $("#print_id").val(stud_id);
  $("#student_name").val(name);
  $("#student_grade").val(grade);
  $("#student_type").val(stud_type);
  $("#grade_id").val(grade_id);
  generate_enroll_id();
  load_sections();
  $("#enroll_student").modal('show');
}

function generate_enroll_id(){
  $.ajax({
  type:"POST",
  url:"generate_enroll_id.php",
  cache:false,
  success:function(data){
    $('#enroll_id').val(data);
  }
  }); 
}

function load_sections(){
  var grade_id, mydata;

  grade_id = document.getElementById('grade_id');
  mydata = 'grade_id=' + grade_id.value;
  $.ajax({
  type:"POST",
  url:"load_sections.php",
  data:mydata,
  cache:false,
  success:function(data){
    $('#load_sections').html(data);
  }
  });
}

function load_section_details(){
  var section_id, mydata;
  section_id = document.getElementById('section_id');
  grade_id = document.getElementById('grade_id');
  mydata = 'section_id=' + section_id.value + '&grade_id=' + grade_id.value;
  $.ajax({
  type:"POST",
  url:"load_section_details.php",
  data:mydata,
  cache:false,
  success:function(data){
    $('#section_details').html(data);
    $('#buttons').removeClass('none');
  }
  });
}

function btn_enroll(){
  var stud_id,enroll_id,section,mydata;
  stud_id = document.getElementById('student_id');
  enroll_id = document.getElementById('enroll_id');
  section = document.getElementById('section_id');
  grade_id = document.getElementById('grade_id');

  mydata = 'student_id=' + student_id.value + '&enroll_id=' + enroll_id.value + '&section_id=' + section.value + '&grade_id=' + grade_id.value;
  $.ajax({
  type:"POST",
  url:"enroll_student.php",
  data:mydata,
  cache:false,
  success:function(data){
    if(data == 1){
      alert("Student Successfully Enrolled.");
      $('#enroll_student').modal('hide');
      load_students();
    }else{
      alert("Error On Enrolling Student.");
    }
  }
  });

}

$(document).ready(function(){
  $('#f_section').val('none');  
  
  $('#enroll_student').on('hidden.bs.modal', function () {
  $("#section_details").html('');
  $("#buttons").addClass('none');
  });
});

function load_enrolled_students(){
  var search = document.getElementById('f_search');
  var sy = document.getElementById('f_sy');
  var grade = document.getElementById('f_grade');
  var section = document.getElementById('f_section');
  var mydata = 'f_search=' + search.value + '&f_sy=' + f_sy.value + '&f_grade=' + grade.value + '&f_section=' + section.value;

  $.ajax({
  type:"POST",
  url:"load_enrolled_students.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_enrolled_students").html('<td colspan="7"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_enrolled_students").html(data);},800); 
  }
  }); 
}

function load_dynamic_section(){
  var grade,mydata;
  grade = document.getElementById('f_grade');

  var mydata = 'f_grade=' + grade.value;

  $.ajax({
    type:'POST',
    url:'load_dynamic_section.php',
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#f_section").html(data);},500);
    }
  });

}

function clear_dropdown(){
  $('#f_grade').val('none');
  $('#f_section').val('none');
}

function student_info(stud_id,name){
$('#student_name').html(name);
$('#s_id').val(stud_id);
load_student_info();
$('#student_info').modal('show');
}

function load_student_info(){
  stud_id = document.getElementById('s_id');

  var mydata = 's_id=' + stud_id.value;

  $.ajax({
    type:'POST',
    url:'load_student_info.php',
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#display_info").html(data);},500);
    }
  });
}

//School Year
function load_school_year(){
  $.ajax({
    type:'POST',
    url:'load_school_year.php',
    cache:false,
    success:function(data){
      setTimeout(function(){$("#load_school_year").html(data);},500);
    }
  });
}

function save_sy(){
  var id,sy,mydata;
  sy = document.getElementById('new_sy');
  id = document.getElementById('sch_id');

  if(sy.value == ''){
    $('#new_sy').focus();
  }else{
    mydata = 'new_sy=' + sy.value + '&sch_id=' + sch_id.value;

    $.ajax({
      type:"POST",
      url:'save_sy.php',
      data:mydata,
      cache:false,
      beforeSend:function(){$("#save_sy").html('<span class="fa fa-spinner fa-spin"></span> Saving...'); $('#save_sy').disable=true;},
      success:function(data){
        if(data == 1){
          $('#add_update_sy').modal('hide');
          load_school_year();
          $('#save_sy').html('<span class="fa fa-save"></span> Save');
          $('#new_sy').val('')
        }
        else if(data == 2){
          $('#add_update_sy').modal('hide');
           $('#save_sy').html('<span class="fa fa-save"></span> Save');
          $('#new_sy').val('')
          load_school_year();
        }else{
          alert('Error While Saving New School Year');
        }
      }
    });

  }

}

function delete_sy(id){
  var mydata = 'id=' + id;
  if(confirm("Are you sure you want to delete this?"))
  {
     $.ajax({
    type:"POST",
    url:"delete_sy.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      load_school_year();
    }
    else if (data == 2) 
    {
      alert('School Year is Active, You cannot delete this school year. To delete this school make sure it is not the active school year');
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

function update_sy(id,sy){
  $('#new_sy').val(sy);
  $('#sch_id').val(id);
  $('#add_update_sy').modal('show');
  $('#sy').html('<span class="fa fa-edit"></span> Update School Year');
}

function set_active(id,sy){
  var mydata = 'id=' + id;
  if(confirm('Are you sure you want' +' '+ sy + ' ' + 'as Active?'))
  {
     $.ajax({
    type:"POST",
    url:"set_active.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 11) 
    {
      load_school_year();
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

//Section
function list_sections(){
  var search,mydata;
  search = document.getElementById('search_sec');
  mydata = 'search_sec=' + search_sec.value;
  $.ajax({
    type:'POST',
    url:'list_sections.php',
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#load_sections").html(data);},500);
    }
  });
}

function save_sec(){
  var id,sec_sy,sec_grade,new_sec,mydata;
  id = document.getElementById('sec_id');
  sec_sy = document.getElementById('sec_sy');
  sec_grade = document.getElementById('sec_grade');
  new_sec = document.getElementById('new_sec');

  if(sec_sy.value == 'none'){
    $('#sec_sy').focus();
  }
  else if(sec_grade.value == 'none'){
    $('#sec_grade').focus();
  }
  else if(new_sec.value == ''){
    $('#new_sec').focus();
  }else{
    mydata = 'sec_id=' + id.value + '&sec_sy=' + sec_sy.value + '&sec_grade=' + sec_grade.value + '&new_sec=' + new_sec.value;

    $.ajax({
      type:"POST",
      url:'save_sec.php',
      data:mydata,
      cache:false,
      beforeSend:function(){$("#save_sec").html('<span class="fa fa-spinner fa-spin"></span> Saving...'); $('#save_sec').disable=true;},
      success:function(data){
        if(data == 1){
          $('#add_update_sec').modal('hide');
          list_sections();
          $('#save_sec').html('<span class="fa fa-save"></span> Save');
          $('#new_sec').val('');
          $('#sec_sy').val('none');
          $('#sec_grade').val('none');
        }
        else if(data == 2){
          $('#add_update_sec').modal('hide');
          list_sections();
          $('#save_sec').html('<span class="fa fa-save"></span> Save');
          $('#new_sec').val('');
          $('#sec_sy').val('none');
          $('#sec_grade').val('none');
        }else{
          alert('Error While Saving Section.');
        }
      }
    });

  }

}

function delete_sec(id){
  var mydata = 'id=' + id;
  if(confirm("Are you sure you want to delete this?"))
  {
     $.ajax({
    type:"POST",
    url:"delete_sec.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      list_sections();
    }
    else if (data == 2) 
    {
      alert('Section is Active, Section is not deletable!');
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

function edit_sec(id,sec,grade,sy){
  $('#sec_id').val(id);
  $('#sec_sy').val(sy);
  $('#sec_grade').val(grade);
  $('#new_sec').val(sec);
  $('#add_update_sec').modal('show');
  $('#sec').html('<span class="fa fa-edit"></span> Update Section');
}

//subject
function list_subjects(){
  var search,mydata;
  search = document.getElementById('search_sub');
  mydata = 'search_sub=' + search.value;
  $.ajax({
    type:'POST',
    url:'list_subjects.php',
    data:mydata,
    cache:false,
    success:function(data){
      setTimeout(function(){$("#load_subjects").html(data);},500);
    }
  });
}

function save_sub(){
  var id,sub_id,sub_type,sub_code,new_sub,mydata;
  id = document.getElementById('sub_id');
  sub_type = document.getElementById('sub_type');
  sub_code = document.getElementById('sub_code');
  new_sub = document.getElementById('new_sub');

  if(sub_type.value == 'none'){
    $('#sub_type').focus();
  }
  else if(sub_code.value == ''){
    $('#sub_code').focus();
  }
  else if(new_sub.value == ''){
    $('#new_sub').focus();
  }else{
    mydata = 'sub_id=' + id.value + '&sub_type=' + sub_type.value + '&sub_code=' + sub_code.value + '&new_sub=' + new_sub.value;

    $.ajax({
      type:"POST",
      url:'save_sub.php',
      data:mydata,
      cache:false,
      beforeSend:function(){$("#save_sub").html('<span class="fa fa-spinner fa-spin"></span> Saving...'); $('#save_sub').disable=true;},
      success:function(data){
        if(data == 1){
          $('#add_update_sub').modal('hide');
          list_subjects();
          $('#save_sub').html('<span class="fa fa-save"></span> Save');
          $('#new_sub').val('');
          $('#sub_type').val('none');
          $('#sub_code').val('');
        }
        else if(data == 2){
          $('#add_update_sub').modal('hide');
          list_subjects();
          $('#save_sub').html('<span class="fa fa-save"></span> Save');
          $('#new_sub').val('');
          $('#sub_type').val('none');
          $('#sub_code').val('');
        }else{
          alert('Error While Saving Subject.');
        }
      }
    });

  }
}

function delete_sub(id){
  var mydata = 'id=' + id;
  if(confirm("Are you sure you want to delete this?"))
  {
     $.ajax({
    type:"POST",
    url:"delete_sub.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      list_subjects();
    }
    else if (data == 2) 
    {
      alert('Subject is Active, subject is not deletable!');
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

function edit_sub(id,sub_code,new_sub,sub_type){
  $('#sub_id').val(id);
  $('#sub_code').val(sub_code);
  $('#new_sub').val(new_sub);
  $('#sub_type').val(sub_type);
  $('#add_update_sub').modal('show');
  $('#sub').html('<span class="fa fa-edit"></span> Update Subject');
}

function get_print_values(){
  get_print_values_sy();
  get_print_values_grade();
  var sy,grade,section; 
  sy = document.getElementById('f_sy');
  grade = document.getElementById('f_grade');
  section = document.getElementById('f_section');
  $('#p_sy').val(sy.value);
  $('#p_grade').val(grade.value);
  $('#p_section').val(section.value);
  enable_button();
}

function get_print_values_grade(){
  var sy,grade; 
  sy = document.getElementById('f_sy');
  grade = document.getElementById('f_grade');
  $('#p_sy2').val(sy.value);
  $('#p_grade2').val(grade.value);
}

function get_print_values_sy(){
  var sy,grade; 
  sy = document.getElementById('f_sy');
  $('#p_sy3').val(sy.value);
}

function enable_button(){
  var sy,grade,section; 
  sy = document.getElementById('p_sy');
  grade = document.getElementById('p_grade');
  section = document.getElementById('p_section');

  if(sy.value == '' && grade.value == '' && section.value == ''){
    document.getElementById('btn_print_all').disabled=true;
    document.getElementById('btn_print_grade').disabled=true;
    document.getElementById('btn_print_sy').disabled=true;
  }else if(sy.value != '' && grade.value != '' && section.value != ''){
    document.getElementById('btn_print_all').disabled=false;
    document.getElementById('btn_print_grade').disabled=true;
    document.getElementById('btn_print_sy').disabled=true;
  }else if(sy.value != '' && grade.value != '' && section.value == ''){
    document.getElementById('btn_print_grade').disabled=false;
    document.getElementById('btn_print_sy').disabled=true;
  }else if(sy.value != '' && grade.value == '' && section.value == ''){
    document.getElementById('btn_print_sy').disabled=false;
  }else{
    document.getElementById('btn_print_all').disabled=true;
    document.getElementById('btn_print_grade').disabled=true;
    document.getElementById('btn_print_sy').disabled=true;
  }
}


function get_age(){
   
  var bdate;
  bdate = document.getElementById('birth_date'); 

  var u_bdate;
  u_bdate = bdate.value;

  mydata = 'birth_date=' + bdate.value;

    $.ajax({
      type: "POST",
      url:"get_age.php",
      data:mydata,
      cache: false,
      success: function (data){
        //alert(data);
        //$("#age").text(data);
        $("#age").val(data);
      }
    });
  }