<?php 
session_start();
if (isset($_SESSION['cashier'])) {
	@header('location:Cashier/');
}
else if (isset($_SESSION['registrar'])) {
    @header('location:Registrar/');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Enrollment System</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.css"/>
  	<script type="text/javascript" src="js/jquery.min.js"></script>
  	<script type="text/javascript" src="js/login.js"></script>
</head>
<body>

<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<a href="index.php" class="	text-sm" >
			<h2><strong class="text-white">Galilee Integrated School</strong></h2> 
			<h3 class="text-left"><small class="w3-text-white">Enrollment System</small></h3></a>
	</div>
</nav>

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6"><br>
			<div class="text-center">
				<img src="img/logo.jpg" width="160">
			</div>
			<div class="col-sm-12 text-center"><h3>Login to start your session.</h3></div>
			<hr style="border:1px solid #2e3d98;">		
			<div>
				<form onsubmit="event.preventDefault(); login_account();">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user fa-lg"></i></span>
						<input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus="">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-key"></i></span>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" autofocus="">
					</div>
				</div>

				<div class="text-right">
					<button id="btn_log" class="btn btn-dark btn-block" style="background: #2e3d98;" onclick="login_account();">Login</button>
				</div>
				</form>
				
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
</html>