function login_account(){
		var username,password;
		username = $("#username").val();
		password = $("#password").val();

		if (username == "") 
		{
			$("#username").focus();
		}
		else if (password == "") 
		{
			$("#password").focus();
		}
		else
		{
			var mydata = 'username=' + username + '&password=' + password;
			$.ajax({
				type:"POST",
				url:"login_process.php",
				data:mydata,
				cache:false,
				success:function(data){
					if (data == 1) 
					{
						window.location="Cashier/";
						$("#username").val('');
 						$("#password").val('');
					}
					else if(data == 2)
					{
						window.location="Registrar/";
						$("#username").val('');
 						$("#password").val('');
					}
					else
					{
						alert("Invalid Account");
					}
				}
			});
		}
	}