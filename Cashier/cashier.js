function generate_or(){
  $.ajax({
  type:"POST",
  url:"generate_or.php",
  cache:false,
  success:function(data){
    $('#or_no').val(data);
  }
  }); 
}

function load_account_students(){
  var search = document.getElementById('search');
  var sy = document.getElementById('sy');
  var mydata = 'search=' + search.value + '&sy=' + sy.value;
  $.ajax({
  type:"POST",
  url:"load_student_accounts.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_student_accounts").html('<td colspan="9"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_student_accounts").html(data);},800); 
  }
  }); 
}

function add_payment(stud_id,balance,name){
  $("#l2_stud_id").val(stud_id);
  $("#l2_stud_name").val(name);
  $("#s_id").val(stud_id);
  $("#balance").val(balance);
  $("#student_name").html(name);
  generate_or();
$("#payment").focus();
  $("#add_payment").modal('show');
  $("#payment").focus();
}

function check_balance(){
  var balance,payment,mydata;
  balance = document.getElementById('balance');
  payment = document.getElementById('payment');

  mydata = 'balance=' + balance.value + '&payment=' + payment.value;
  $.ajax({
      type:"POST",
      url:'check_balance.php',
      data:mydata,
      cache:false,
      success:function(data){
        if(data == 1){
          document.getElementById('btn_save').disabled = true;
          $('#warning').html('<span class="fa fa-warning"></span> Please make sure the payment is <br>equal or less than on the balance.');
        }else{
          $('#warning').html('');
          document.getElementById('btn_save').disabled = false;
        }
      }
    });
}

function check_payment_type(){
  var balance,payment_type;
  balance = $('#balance').val();
  payment_type = $('#payment_type').val();

  if(payment_type == 1){
    document.getElementById('payment').disabled=false;
    document.getElementById('btn_save').disabled=true;
    $('#payment').val('');
    $('#payment').focus();

  }else if (payment_type == 2){
    $('#payment').val(balance);
    document.getElementById('payment').disabled=true;
    document.getElementById('btn_save').disabled=false;

  }

}

function save_payment(){
  var stud_id,balance,or_no,payment,payment_type,mydata;
  stud_id = document.getElementById('s_id');
  balance = document.getElementById('balance');
  or_no = document.getElementById('or_no');
  payment = document.getElementById('payment');
  payment_type = $('#payment_type').val();

  if(payment_type.value = ''){
    $('#payment_type').focus();
  }
  else if(payment.value == ''){
    $('#payment').focus();
  }else{

    mydata = 's_id=' + stud_id.value + '&balance=' + balance.value + '&or_no=' + or_no.value + '&payment=' + payment.value + '&payment_type=' + payment_type;
    $.ajax({
      type:"POST",
      url:'save_payment.php',
      data:mydata,
      cache:false,
      beforeSend:function(){$("#btn_save").html('<span class="fa fa-spinner fa-spin"></span> Saving payment...'); $('#save_sy').disable=true;},
      success:function(data){
        if(data == 1){
          $('#add_payment').modal('hide');
          load_account_students();
          generate_or();
          $('#btn_save').html('<span class="fa fa-save"></span> Save');
          $('#or_no').val('');
          $('#payment').val('');
          $('#balance').val('');
          $('#payment_type').val('');
          $('#student_name').html('');
          $('#btn_submit').click();
        }else{
          alert('Error While Saving Payment');
        }
      }
    });
  }
}

function payment_details(stud_id,name){
  $('#p_id').val(stud_id);
  $('#print_id').val(stud_id);
  $('#p_name').html(name);
  load_p_details();
  $('#payment_details').modal('show');
}

function load_p_details(){
  var stud_id = document.getElementById('p_id');
  var search_p = document.getElementById('search_p');
  var mydata = 'p_id=' + stud_id.value + '&search_p=' + search_p.value;
  $.ajax({
  type:"POST",
  url:"load_payment_details.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_payment_details").html('<td colspan="9"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_payment_details").html(data);},800); 
  }
  });
}

function get_last_trans(id,or_no){
  $('#d_id').val(id);
  $('#d_or_no').html(or_no);
  $('#delete_last_trans').modal('show');
}

function verify_pass(){
  var pass,mydata;
  pass = document.getElementById('credential');

  if(pass.value == ''){
    $('#credential').focus();
  }else{
    mydata = 'credential=' + pass.value;
    $.ajax({
      url: "check_credential.php",
      type: 'POST',
      data:mydata,
      cache:false,
      success:function(data){
        if(data == 1){
          $('.del').removeClass('hide');
          $('#cred').addClass('hide');
          document.getElementById('btn_del').disabled=false;
          $('#btn_del').focus();
        }else{
          alert('Wrong credential');
           $('#credential').focus();
        }
      }
    });
  }
}

function delete_transaction(){
  var id = document.getElementById('d_id');
  var mydata = 'id=' + id.value;
  if(confirm("Are you sure you want to delete this transaction?"))
  {
     $.ajax({
    type:"POST",
    url:"delete_trans.php",
    data:mydata,
    cache:false,
    success:function(data){
    if (data == 1) 
    {
      alert('Transaction Successfully Deleted.');
      $('#delete_last_trans').modal('hide');
      $('.del').addClass('hide');
      $('#cred').removeClass('hide');
      $('#credential').val('');
      document.getElementById('btn_del').disabled=true;
      load_p_details();
      load_account_students();
    }
    else
    {
    alert(data);
    }
    }
    });
  }
}

$(document).ready(function(){
  $('#delete_last_trans').on('hidden.bs.modal', function () {
  $('.del').addClass('hide');
  $('#cred').removeClass('hide');
  $('#credential').val('');
  document.getElementById('btn_del').disabled=true;
  });
});

function load_billings(){
  var search = document.getElementById('search_b');
  var mydata = 'search_b=' + search.value;
  $.ajax({
  type:"POST",
  url:"load_billings.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_billings").html('<td colspan="10"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_billings").html(data);},800); 
  }
  }); 
}

function get_billing(id,grade,tuition,reg,misc,comp,aircon,books,total){
  $('#grade_name').html(grade);
  $('#b_id').val(id);
  $('#tuition').val(tuition);
  $('#reg_fee').val(reg);
  $('#misc_fee').val(misc);
  $('#computer').val(comp);
  $('#aircon').val(aircon);
  $('#books').val(books);
  $('#total').val(total);
  $('#update_billing').modal('show');
}

function auto_compute(){
  var tuition,reg,misc,comp,aircon,books,mydata;
  tuition = $('#tuition').val();
  reg = $('#reg_fee').val();
  misc = $('#misc_fee').val();
  comp = $('#computer').val();
  aircon = $('#aircon').val();
  books = $('#books').val();

  mydata = 'tuition=' + tuition + '&reg_fee=' + reg + '&misc_fee=' + misc + '&computer=' + comp + '&aircon=' + aircon + '&books=' + books ;

  $.ajax({
  type:"POST",
  url:"auto_compute.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#total").val('Computing...');},
  success:function(data){
  setTimeout(function(){$("#total").val(data);},800); 
  }
  });
}

function update_billing(){
  var b_id,tuition,reg,misc,comp,aircon,books,total,mydata;
  b_id = $('#b_id').val();
  tuition = $('#tuition').val();
  reg = $('#reg_fee').val();
  misc = $('#misc_fee').val();
  comp = $('#computer').val();
  aircon = $('#aircon').val();
  books = $('#books').val();
  total = $('#total').val();

  mydata = 'b_id=' + b_id + '&tuition=' + tuition + '&reg_fee=' + reg + '&misc_fee=' + misc + '&computer=' + comp + '&aircon=' + aircon + '&books=' + books + '&total=' + total;

  if(tuition == ''){
    $('#tuition').focus();
  }else if(reg == ''){
    $('#reg_fee').focus();
  }else if(misc == ''){
    $('#misc_fee').focus();
  }else if(comp == ''){
    $('#computer').focus();
  }
  else if(aircon == ''){
    $('#aircon').focus();
  }
  else if(books == ''){
    $('#books').focus();
  }else{
    $.ajax({
    type:"POST",
    url:"update_billing.php",
    data:mydata,
    cache:false,
    beforeSend:function(){$("#btn_update").html('<span class="fa fa-spin fa-spinner"></span> Updating...');},
    success:function(data){
    
      if(data == 1){
        load_billings();
        $('#b_id').val('');
        $('#tuition').val('');
        $('#reg_fee').val('');
        $('#misc_fee').val('');
        $('#computer').val('');
        $('#aircon').val('');
        $('#books').val('');
        $('#total').val('');
        $('#update_billing').modal('hide');
        alert('Billing Successfully Updated...');
        $("#btn_update").html('<span class="fa fa-save"></span> Save');
      }else{
        alert(data);
      }

    }
    });
  } 
}

function get_discounts(id,cash,a,b,c,d,e,f){
  $('#grade_name2').html(f);
  $('#d_id').val(id);
  $('#cash').val(cash);
  $('#2_child').val(a);
  $('#3_child').val(b);
  $('#4_child').val(c);
  $('#old_student').val(d);
  $('#j_hs').val(e);
  $('#update_discounts').modal('show');
}

function update_discounts(){
  var d_id,cash,a,b,c,d,e,f;
  d_id = $('#d_id').val();
  cash = $('#cash').val();
  a = $('#2_child').val();
  b = $('#3_child').val();
  c = $('#4_child').val();
  d = $('#old_student').val();
  e = $('#j_hs').val();

  mydata = 'd_id=' + d_id + '&cash=' + cash + '&2_child=' + a + '&3_child=' + b + '&4_child=' + c + '&old_student=' + d + '&j_hs=' + e;

  if(cash == ''){
    $('#cash').focus();
  }else if(a == ''){
    $('#2_child').focus();
  }else if(b == ''){
    $('#3_child').focus();
  }else if(c == ''){
    $('#4_child').focus();
  }
  else if(d == ''){
    $('#old_student').focus();
  }
  else if(e == ''){
    $('#j_hs').focus();
  }
  else{
    $.ajax({
    type:"POST",
    url:"update_discount.php",
    data:mydata,
    cache:false,
    beforeSend:function(){$("#btn_discount").html('<span class="fa fa-spin fa-spinner"></span> Updating...');},
    success:function(data){
    
      if(data == 1){
        load_billings();
        $('#d_id').val('');
        $('#cash').val('');
        $('#2_child').val('');
        $('#3_child').val('');
        $('#4_child').val('');
        $('#old_student').val('');
        $('#j_hs').val('');
        $('#update_discounts').modal('hide');
        alert('Discounts Successfully Updated...');
        $("#btn_discount").html('<span class="fa fa-save"></span> Save');
      }else{
        alert(data);
      }

    }
    });
  } 
}

function load_collections(){
  var search = document.getElementById('search_c');
  var mydata = 'search_c=' + search.value;
  $.ajax({
  type:"POST",
  url:"load_collections.php",
  data:mydata,
  cache:false,
  beforeSend:function(){$("#load_collections").html('<td colspan="9"><div class="col-lg-12 text-center"><span class="fa fa-spinner fa-spin"></span></div></td>');},
  success:function(data){
  setTimeout(function(){$("#load_collections").html(data);},800); 
  }
  }); 
}