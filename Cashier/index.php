<?php 
session_start();
if (!isset($_SESSION['cashier'])) {
	@header('location:../');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cashier</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>

  	<style type="text/css">
.sidenav {
	height: 100%;
	width: 150px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #f1f1f1;
	overflow-x: hidden;
	padding-top: 20px;
}

.sidenav ul{
	padding-top: 50px;
	padding-left: 0;
}

.sidenav ul li{
	text-align: center;
	padding: 20px;
	border-bottom: 2px solid #2e3d98;
}

.sidenav ul li:first-child{
	border-top: 2px solid #2e3d98;
}

.sidenav a:hover{
	text-decoration: none;
}

@media only screen and (max-width: 600px) {
    .btn-menu{
    	width: 100%;
    	text-align: left;
    }
  }

.logo{
    width: 350px; /* You can set the dimensions to whatever you want */
    height: 350px;
    object-fit: cover;
}

</style>



</head>
<body>

<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System - <small><?php echo $_SESSION['user_level'] ?></small></strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='billing.php'"><span class="fa fa-money fa-2x"></span> Student Billing</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='collection.php'"><span class="fa fa-money fa-2x"></span> Collection</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='manage_billing.php'"><span class="fa fa-edit fa-2x"></span> Manage School Fee's</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid">
	<div class="col-lg-12 text-center"><br><br>
		<img src="../img/logo.jpg" class="img-responsive text-center logo">
	</div>
	
	<!--<div class="row">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Manage Enrollment</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<a data-toggle="modal" data-target="#add_user"><button type="button" class="btn btn-dark btn-normal"><span class="fa fa-user-plus fa-lg"></span> Student Form</button></a>
				</div>
				<div class="col-lg-10 input">
					<div class="input-group">
		                <input type="show" class="form-control" oninput="mymusic();" name="mymusic_search" id="mymusic_search" placeholder="Search Student...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div>
			</div><hr>
		</div>
	</div>-->
</div>
</body>
</html>