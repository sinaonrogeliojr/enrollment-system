<?php 
session_start();
if (!isset($_SESSION['cashier'])) {
	@header('location:../');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cashier</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="cashier.js"></script>
  	<script type="text/javascript" src="w3.js"></script>

  	<style type="text/css">
.sidenav {
	height: 100%;
	width: 150px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #f1f1f1;
	overflow-x: hidden;
	padding-top: 20px;
}

.sidenav ul{
	padding-top: 50px;
	padding-left: 0;
}

.sidenav ul li{
	text-align: center;
	padding: 20px;
	border-bottom: 2px solid #2e3d98;
}

.sidenav ul li:first-child{
	border-top: 2px solid #2e3d98;
}

.sidenav a:hover{
	text-decoration: none;
}

@media only screen and (max-width: 600px) {
    .btn-menu{
    	width: 100%;
    	text-align: left;
    }
  }

.logo{
    width: 350px; /* You can set the dimensions to whatever you want */
    height: 350px;
    object-fit: cover;
}

.modal-medium{
	max-width:60%;
    width: 100%;
}

.hide{
	display: none;
}
</style>



</head>
<body onload="load_billings();">

<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System - <small><?php echo $_SESSION['user_level'] ?></small></strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='billing.php'"><span class="fa fa-money fa-2x"></span> Student Billing</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='collection.php'"><span class="fa fa-money fa-2x"></span> Collection</button>
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='manage_billing.php'"><span class="fa fa-edit fa-2x"></span> Manage School Fee's</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	
	<div class="row">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Manage Billing</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 input">
					<div class="input-group">
		                <input type="show" oninput="load_billings();" class="form-control" oninput="" name="search_b" id="search_b" placeholder="Search Student...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div>
			</div>
		</div>
	</div><br>
		<div class="table table-responsive">
			<table class="table table-hover table-sm table-striped table-bordered" id="billing">
				<thead>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(1)')" style="cursor:pointer">GRADE</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(2)')" style="cursor:pointer">Tuition Fee</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(3)')" style="cursor:pointer">Registration Fee</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(4)')" style="cursor:pointer">Miscellaneus</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(5)')" style="cursor:pointer">Laboratory Fees</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(6)')" style="cursor:pointer">Aircon</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(7)')" style="cursor:pointer">Books</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(8)')" style="cursor:pointer">Total</th>
					<th>Discounts</th>
					<th>Update</th>
				</thead>
				<tbody id="load_billings">
					
				</tbody>
			</table>
		</div>
</div>
<!-- Update Billing-->
<div class="modal fade" id="update_billing">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-money"></span> Update Billing: <strong id="grade_name"></strong></p>
				<input type="hidden" id="b_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Tuition Fee:</label>
				            <input class="form-control" oninput="auto_compute();" id="tuition" type="number" min="0" placeholder="Enter Tution Fee">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Registration Fee:</label>
				            <input class="form-control" oninput="auto_compute();" id="reg_fee" name="reg_fee" type="number" min="0" placeholder="Enter Registration Fee">
				        </div>
					</div>
				</div>

				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Miscellaneus Fee:</label>
				            <input min='0' type="number" oninput="auto_compute();" class="form-control" id="misc_fee" placeholder="Enter Miscellaneus Fee">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Computer:</label>
				            <input min='0' type="number" oninput="auto_compute();" class="form-control" id="computer" placeholder="Enter Computer Fee">
				        </div>
					</div>
				</div>
				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Aircon:</label>
				            <input min='0' type="number" oninput="auto_compute();" class="form-control" id="aircon" placeholder="Enter Aircon Fee">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Books:</label>
				            <input min='0' type="number" oninput="auto_compute();" class="form-control" id="books" placeholder="Enter Aircon Fee">
				        </div>
					</div>
				</div>
		        <div class="col-sm-12">
		        	<div class="form-group">
					<label for="fn" class="w3-small">Total:</label>
		            <input min='0' style="background: transparent; border: none; font-weight: bolder; color:red; font-size: 20px;" type="text" class="form-control" disabled="" id="total">
		        </div>
		        </div>
		        
			</div>
			<div class="modal-footer none" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-default" id="btn_update" onclick="update_billing();"><span class="fa fa-save"></span> Save</button>
				
			</div>
		</div>
	</div>
</div>

<!-- Update Discounts-->
<div class="modal fade" id="update_discounts">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-percent"></span> Update Discounts: <strong id="grade_name2"></strong></p>
				<input type="hidden" id="d_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Cash:</label>
				            <input class="form-control" id="cash" type="number" min="0" placeholder="Enter Discount">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">2 Children:</label>
				            <input class="form-control" id="2_child" type="number" min="0" placeholder="Enter Discount">
				        </div>
					</div>
				</div>

				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">3 Children:</label>
				            <input min='0' type="number" class="form-control" id="3_child" placeholder="Enter Discount">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">4 Children:</label>
				            <input min='0' type="number" class="form-control" id="4_child" placeholder="Enter Discount">
				        </div>
					</div>
				</div>
				<div class="col-sm-12 row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Old Student:</label>
				            <input min='0' type="number" class="form-control" id="old_student" placeholder="Enter Discount">
				        </div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="fn" class="w3-small">Junior HS:</label>
				            <input min='0' type="number" class="form-control" id="j_hs" placeholder="Enter Discount">
				        </div>
					</div>
				</div>
			</div>
			<div class="modal-footer none" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-default" id="btn_discount" onclick="update_discounts();"><span class="fa fa-save"></span> Save</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>