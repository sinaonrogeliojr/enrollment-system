<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search_b']);

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT t2.`grade_level`,t1.* FROM tbl_billings t1 LEFT JOIN tbl_grade_level t2 ON t1.`grade_id` = t2.`id`");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr class="item">
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo number_format($row['tuition_fee']); ?></td>
				<td><?php echo number_format($row['reg_fee']); ?></td>
				<td><?php echo number_format($row['misc_fee']); ?></td>
				<td><?php echo number_format($row['computer']); ?></td>
				<td><?php echo number_format($row['aircon']); ?></td>
				<td><?php echo number_format($row['books']); ?></td>
				<td><strong class="text-success"><?php echo number_format($row['total']); ?></strong></td>
				<td><button class="btn btn-info btn-sm" onclick="get_discounts('<?php echo $row['id'] ?>','<?php echo $row['discount_cash'] ?>','<?php echo $row['discount_2ndchild'] ?>','<?php echo $row['discount_3rdchild'] ?>','<?php echo $row['discount_4thchild'] ?>','<?php echo $row['discount_oldstudent'] ?>','<?php echo $row['discount_gis'] ?>','<?php echo $row['grade_level'] ?>')" title="View Discount Details">
							<span class="fa fa-percent"></span> Edit Discounts
						</button>
				</td>	
				<td>
					<button class="btn btn-default btn-sm" onclick="get_billing('<?php echo $row['id'] ?>','<?php echo $row['grade_level'] ?>','<?php echo $row['tuition_fee'] ?>','<?php echo $row['reg_fee'] ?>','<?php echo $row['misc_fee'] ?>','<?php echo $row['computer'] ?>','<?php echo $row['aircon'] ?>','<?php echo $row['books'] ?>','<?php echo $row['total'] ?>');" title="Update Billing">
							<span class="fa fa-money"></span> Edit Fee's
					</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="10">No Billing Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT t2.`grade_level`,t1.* FROM tbl_billings t1 LEFT JOIN tbl_grade_level t2 ON t1.`grade_id` = t2.`id` Where t2.`grade_level` like '%$search%'");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr class="item">
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo number_format($row['tuition_fee']); ?></td>
				<td><?php echo number_format($row['reg_fee']); ?></td>
				<td><?php echo number_format($row['misc_fee']); ?></td>
				<td><?php echo number_format($row['computer']); ?></td>
				<td><?php echo number_format($row['aircon']); ?></td>
				<td><?php echo number_format($row['books']); ?></td>
				<td><?php echo number_format($row['total']); ?></td>
				<td><button class="btn btn-info btn-sm" onclick="discount('<?php echo $row['id'] ?>')" title="View Discount Details">
							<span class="fa fa-info"></span> Discount
						</button>
				</td>	
				<td>
					<button class="btn btn-default btn-sm" onclick="get_billing('<?php echo $row['id'] ?>');" title="Update Billing">
							<span class="fa fa-edit"></span> Update
					</button>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="10">No Billing Found!</td>';
   }

}

?>