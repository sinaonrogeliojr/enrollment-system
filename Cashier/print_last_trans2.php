<?php 
include('../db_config.php');
$stud_id = $_POST['l2_stud_id'];
$name = $_POST['l2_stud_name'];
$sql = mysqli_query($con, "SELECT stud_id,TYPE,payment,balance,date_trans,Or_number FROM tbl_student_ledger WHERE stud_id = '$stud_id' AND id IN (SELECT MAX(id) FROM tbl_student_ledger WHERE stud_id = '$stud_id' GROUP BY stud_id)");
$row = mysqli_fetch_assoc($sql);

$stud_details = mysqli_query($con, "SELECT * FROM tbl_enrollment_form where id = '$stud_id'");
$rows = mysqli_fetch_assoc($stud_details);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Student</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/print.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="col-lg-12 text-center">
		<img src="../img/logo.jpg" class="img-responsive" width="120px" height="120px">
		<h4>Galilee Integraded School</h4>
		<hr>
		<div class="col-lg-12 row">
			<div class="col-lg-1"></div>
			<div class="col-lg-7">
				<table>
					<tr>
						<td class="text-left">Student ID: &nbsp;</td>
						<td class="text-left"><b><?php echo $rows['stud_id']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">Student Name: &nbsp;</td>
						<td class="text-left"><b><?php echo $name; ?></b></td>
					</tr>
					<tr>
						<td class="text-left"> Address: &nbsp;</td>
						<td class="text-left"><b><?php echo $rows['stud_address']; ?></b></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4 float-right">
				<table>
					<tr>
						<td class="text-right">OR#: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['Or_number'] ?></b></td>
					</tr>
					<tr>
						<td class="text-right">Date: &nbsp;</td>
						<td class="text-left"><b><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></b></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="col-lg-12 row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4"><table class="table table-sm">
				<tr>
					<td class="text-right">Type: &nbsp;</td>
					<td class="text-left"><b><?php echo $row['TYPE']; ?></b></td>
				</tr>
				<tr>
					<td class="text-right">Amount: &nbsp;</td>
					<td class="text-left"><b><?php echo $row['payment']; ?></b></td>
				</tr>
				<tr>
					<td class="text-right">Balance: &nbsp;</td>
					<td class="text-left"><b><?php echo $row['balance']; ?></b></td>
				</tr>
			</table></div>
			<div class="col-lg-4"></div>
			
		</div><br>
		<div class="col-lg-12 row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4" style="line-height: 15px;">
				<hr style="border: 1px solid #000;">
				Claudine Videz<br>
				Cashier
			</div>
		</div><br>
		<div class="col-lg-12" id="btns">
			<button class="btn btn-sm btn-default" onclick="print_preview();"><span class="fa fa-print"></span> Preview</button>
			<button class="btn btn-sm btn-default" onclick="window.location='billing.php'"> Back</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	function print_preview(){
		$('#btns').addClass('none');
		window.print();
		$('#btns').removeClass('none');
		//window.location = 'billing.php';
	}
</script>
</body>
</html>