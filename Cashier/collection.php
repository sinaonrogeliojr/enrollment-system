<?php 
session_start();
if (!isset($_SESSION['cashier'])) {
	@header('location:../');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cashier</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="cashier.js"></script>
  	<script type="text/javascript" src="w3.js"></script>

  	<style type="text/css">
.sidenav {
	height: 100%;
	width: 150px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #f1f1f1;
	overflow-x: hidden;
	padding-top: 20px;
}

.sidenav ul{
	padding-top: 50px;
	padding-left: 0;
}

.sidenav ul li{
	text-align: center;
	padding: 20px;
	border-bottom: 2px solid #2e3d98;
}

.sidenav ul li:first-child{
	border-top: 2px solid #2e3d98;
}

.sidenav a:hover{
	text-decoration: none;
}

@media only screen and (max-width: 600px) {
    .btn-menu{
    	width: 100%;
    	text-align: left;
    }
  }

.logo{
    width: 350px; /* You can set the dimensions to whatever you want */
    height: 350px;
    object-fit: cover;
}

.modal-medium{
	max-width:60%;
    width: 100%;
}

.hide{
	display: none;
}
</style>

</head>
<body onload="load_collections();">

<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System - <small><?php echo $_SESSION['user_level'] ?></small></strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='billing.php'"><span class="fa fa-money fa-2x"></span> Student Billing</button>
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='collection.php'"><span class="fa fa-money fa-2x"></span> Collection</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='manage_billing.php'"><span class="fa fa-edit fa-2x"></span> Manage School Fee's</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	
	<div class="row">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Collection For the day</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 input">
					<div class="input-group">
		                <input type="show" oninput="load_collections();" class="form-control" oninput="" name="search_c" id="search_c" placeholder="Search Collection...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div>
				<div class="col-sm-2">
					<button class="btn btn-default" onclick="window.location='print_collection.php'"><span class="fa fa-print"></span> Print Collection</button>
				</div>
			</div>
		</div>
	</div><br>
		<div class="table table-responsive">
			<table class="table table-hover table-sm table-striped table-bordered" id="billing">
				<thead>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(1)')" style="cursor:pointer">OR #</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(2)')" style="cursor:pointer">Student ID</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(3)')" style="cursor:pointer">Student</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(4)')" style="cursor:pointer">Grade</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(5)')" style="cursor:pointer">Section</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(6)')" style="cursor:pointer">Type</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(7)')" style="cursor:pointer">Payment</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(8)')" style="cursor:pointer">Date</th>
				</thead>
				<tbody id="load_collections">
					
				</tbody>
			</table>
		</div>
</div>
</body>
</html>