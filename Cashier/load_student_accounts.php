<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search']);
$sy = mysqli_real_escape_string($con, $_POST['sy']);

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT t1.`stud_id`,t2.`stud_id` as studentId,CONCAT(t2.`ln`,', ', t2.`fn`, ' ', t2.`mn`) AS NAME,t3.`grade_level`,t4.`section`,t5.`school_year`,t1.`TYPE`,t1.`payment`,t1.`balance`,t1.`date_trans`,t1.`Or_number` FROM tbl_student_ledger t1 
		LEFT JOIN tbl_enrollment_form t2 ON t1.`stud_id` = t2.`id` 
		LEFT JOIN tbl_grade_level t3 ON t2.`grade_level` = t3.`id`
		LEFT JOIN tbl_section t4 ON t2.`section_id` = t4.`id`
		LEFT JOIN tbl_school_year t5 ON t2.`school_year` = t5.`id` 
		WHERE t1.`id` IN (SELECT MAX(id) FROM tbl_student_ledger GROUP BY stud_id) and t1.`school_year` = '$sy' ORDER BY t2.`ln` ASC;");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr class="item">
				<td><?php echo $row['studentId']; ?></td>
				<td><?php echo $row['NAME']; ?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo $row['section']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td><label class="text-danger"><?php echo $row['balance']; ?></label></td>
				<td><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></td>
				<td>
					<?php 
					$status='';

					if($row['balance'] == 0.00){
						$status = '<label class="text-success"> Fully Paid </label>';
					}else{
						$status = '<label class="text-danger"> Not Paid </label>';
					}
					echo $status;
					?>
					
				</td>
				<td>
					<div class="btn btn-group btn-justify">
						<button class="btn btn-info btn-sm" onclick="payment_details('<?php echo $row['stud_id'] ?>','<?php echo $row['NAME'] ?>');" title="View Payment Details">
							<span class="fa fa-info"></span>
						</button>
						<?php 
						if($row['balance'] == 0.00){ ?>
						<button class="btn btn-success btn-sm" disabled="" title="Fully Paid"><span class="fa fa-check"></span> </button>
					<?php }else{ ?>
						<button class="btn btn-default btn-sm" title="Add New Payment" onclick="add_payment('<?php echo $row['stud_id'] ?>','<?php echo $row['balance'] ?>','<?php echo $row['NAME'] ?>')"><span class="fa fa-plus"></span> </button>
					<?php	
						}
					?>
					<form action="print_last_trans.php" method="post">
						<input type="text" style="display: none" id="l_stud_id" name="l_stud_id" value="<?php echo $row['stud_id']; ?>" autocomplete="off"/>
						<input type="text" style="display: none" id="l_stud_name" name="l_stud_name" value="<?php echo $row['NAME']; ?>" autocomplete="off"/>
						<button class="btn btn-dark btn-sm" title="Print Last Transaction" type="submit" id="l_btn_print"><span class="fa fa-print"></span> 
						</button>  
					</form>
					</div>
					
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="9">No Student Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT t1.`stud_id`,CONCAT(t2.`ln`,', ', t2.`fn`, ' ', t2.`mn`) AS NAME,t3.`grade_level`,t4.`section`,t5.`school_year`,t1.`TYPE`,t1.`payment`,t1.`balance`,t1.`date_trans`,t1.`Or_number` FROM tbl_student_ledger t1 
		LEFT JOIN tbl_enrollment_form t2 ON t1.`stud_id` = t2.`stud_id` 
		LEFT JOIN tbl_grade_level t3 ON t2.`grade_level` = t3.`id`
		LEFT JOIN tbl_section t4 ON t2.`section_id` = t4.`id`
		LEFT JOIN tbl_school_year t5 ON t2.`school_year` = t5.`id` 
		WHERE t2.`fn` like '%$search%' and t1.`id` IN (SELECT MAX(id) FROM tbl_student_ledger GROUP BY stud_id) and t1.`school_year` = '$sy' ORDER BY t2.`ln` ASC;");


	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			
			<tr class="item">
				<td><?php echo $row['stud_id']; ?></td>
				<td><?php echo $row['NAME']; ?></td>
				<td><?php echo $row['grade_level']; ?></td>
				<td><?php echo $row['section']; ?></td>
				<td><?php echo $row['school_year']; ?></td>
				<td><label class="text-danger"><?php echo $row['balance']; ?></label></td>
				<td><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></td>
				<td>
					<?php 
					$status='';

					if($row['balance'] == 0.00){
						$status = '<label class="text-success"> Fully Paid </label>';
					}else{
						$status = '<label class="text-danger"> Not Paid </label>';
					}
					echo $status;
					?>
					
				</td>
				<td>
					<div class="btn btn-group btn-justify">
						<button class="btn btn-info btn-sm" onclick="payment_details('<?php echo $row['stud_id'] ?>','<?php echo $row['NAME'] ?>');" title="View Payment Details">
							<span class="fa fa-info"></span>
						</button>
						<?php 
						if($row['balance'] == 0.00){ ?>
						<button class="btn btn-success btn-sm" disabled="" title="Fully Paid"><span class="fa fa-check"></span> </button>
					<?php }else{ ?>
						<button class="btn btn-default btn-sm" title="Add New Payment" onclick="add_payment('<?php echo $row['stud_id'] ?>','<?php echo $row['balance'] ?>','<?php echo $row['NAME'] ?>')"><span class="fa fa-plus"></span> </button>
					<?php	
						}
					?>
					<form action="print_last_trans.php" method="post">
						<input type="text" style="display: none" id="l_stud_id" name="l_stud_id" value="<?php echo $row['stud_id']; ?>" autocomplete="off"/>
						<input type="text" style="display: none" id="l_stud_name" name="l_stud_name" value="<?php echo $row['NAME']; ?>" autocomplete="off"/>
						<button class="btn btn-dark btn-sm" title="Print Last Transaction" type="submit" id="l_btn_print"><span class="fa fa-print"></span> 
						</button>  
					</form>
					</div>
					
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="9">No Student Found!</td>';
   }

}

?>