<?php 
session_start();
if (!isset($_SESSION['cashier'])) {
	@header('location:../');
}

include('../db_config.php');
function school_year($con){
    $sql = mysqli_query($con,"SELECT * from tbl_school_year order by status ASC");
    if (mysqli_num_rows($sql)>0) {
        while ($row = mysqli_fetch_assoc($sql)) {
        echo '<option value="'.$row['school_year'].'">'.$row['school_year'].'</option>';
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cashier</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="cashier.js"></script>
  	<script type="text/javascript" src="w3.js"></script>

  	<style type="text/css">
.sidenav {
	height: 100%;
	width: 150px;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: #f1f1f1;
	overflow-x: hidden;
	padding-top: 20px;
}

.sidenav ul{
	padding-top: 50px;
	padding-left: 0;
}

.sidenav ul li{
	text-align: center;
	padding: 20px;
	border-bottom: 2px solid #2e3d98;
}

.sidenav ul li:first-child{
	border-top: 2px solid #2e3d98;
}

.sidenav a:hover{
	text-decoration: none;
}

@media only screen and (max-width: 600px) {
    .btn-menu{
    	width: 100%;
    	text-align: left;
    }
  }

.logo{
    width: 350px; /* You can set the dimensions to whatever you want */
    height: 350px;
    object-fit: cover;
}

.modal-medium{
	max-width:60%;
    width: 100%;
}

.hide{
	display: none;
}
</style>



</head>
<body onload="load_account_students();">

<nav class="navbar navbar-default" style="background: #2e3d98;">
	<div class="container-fluid text-center">
		<div class="col-lg-6">
			<a href="index.php">
				<h2 class="text-left"><strong class="text-white">Enrollment System - <small><?php echo $_SESSION['user_level'] ?></small></strong></h2> 
			</a>
		</div>
		<div class="col-lg-6 text-right text-white">
			 <p><span>Hello! <a href="#" title="View Profile"><span id="img_pf"></span> <?php echo $_SESSION['username']; ?></a></span></p>
		</div>
	</div>
</nav>
<nav class="navbar navbar-default" style="background: #f2f2f2;">
	<div class="container-fluid text-center">
		<div class="col-sm-12 row">
			<div class="col-sm-1"></div>
			<div class="col-sm-10 ">
				<div class="btn-group btn-group-justified">
					<button class="btn btn-lg btn-success btn-menu active" onclick="window.location='billing.php'"><span class="fa fa-money fa-2x"></span> Student Billing</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='collection.php'"><span class="fa fa-money fa-2x"></span> Collection</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='manage_billing.php'"><span class="fa fa-edit fa-2x"></span> Manage School Fee's</button>
					<button class="btn btn-lg btn-success btn-menu" onclick="window.location='../logout.php'"><span class="fa fa-sign-out fa-2x"></span> Exit</button></div>
				</div>
			
			<div class="col-sm-1"></div>
		</div>
		
	</div>
</nav>

<div class="container-fluid" style="padding: 20px;">	
	<div class="row">
			<div class="col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<p class="w3-large">Student's Ledger</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-2">
					<div class="form-group">
			            <select id="sy" class="form-control select_form" oninput="load_account_students();">
			            	<option value="0" selected="" disabled="">--Select School Year--</option>
			            	<?php echo school_year($con); ?>
			            </select>
			         </div>
				</div>
				<div class="col-lg-4 input">
					<div class="input-group">
		                <input type="show" oninput="load_account_students();" class="form-control" oninput="" name="search" id="search" placeholder="Search Student...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div>
			</div>
		</div>
	</div><br>
		<div class="table table-responsive">
			<table class="table table-hover table-sm table-striped table-bordered" id="billing">
				<thead>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(1)')" style="cursor:pointer">Student ID</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(2)')" style="cursor:pointer">StudentName</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(3)')" style="cursor:pointer">Grade</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(4)')" style="cursor:pointer">Section</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(5)')" style="cursor:pointer">School Year</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(6)')" style="cursor:pointer">Balance</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(7)')" style="cursor:pointer">Date Trans</th>
					<th onclick="w3.sortHTML('#billing', '.item', 'td:nth-child(8)')" style="cursor:pointer">Status</th>
					<th>Action</th>
				</thead>
				<tbody id="load_student_accounts">
					
				</tbody>
			</table>
		</div>
</div>
<!-- Add Payment-->
<div class="modal fade" id="add_payment">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-money"></span> Add Payment to: <strong id="student_name"></strong></p>
				<input type="hidden" id="s_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
					<label for="fn" class="w3-small">Balance:</label>
		            <input class="form-control" id="balance" name="balance" disabled="" type="text" placeholder="Balance">
		        </div>
		        <div class="form-group">
					<label for="payment_type" class="w3-small">Payment Type:
					</label>
			        <select id="payment_type" class="form-control" onchange="check_payment_type();">
			        	<option selected="" value="0">Select Type</option>
			        	<option value="1">Partial Payment</option>
			        	<option value="2">Full Payment</option>
			        </select>
			     </div>
		        <div class="form-group">
					<label for="fn" class="w3-small">Or number:</label>
		            <input class="form-control" id="or_no" name="or_no" type="text" disabled="" placeholder="Enter Or Number">
		        </div>
		        <div class="form-group">
					<label for="fn" class="w3-small">New Payment:</label>
		            <input min='0' type="number" oninput="check_balance();" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" id="payment" name="payment" placeholder="Enter New Payment">
		        </div>
			</div>
			<div class="modal-footer none" id="buttons">
				<label class="badge badge-danger text-sm" id="warning"></label>
				<button class="btn btn-default" id="btn_save" disabled="" onclick="save_payment();"><span class="fa fa-save"></span> Save</button>
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				
				<form action="print_last_trans2.php" method="post">
						<input type="text" style="display: none;" id="l2_stud_id" name="l2_stud_id" autocomplete="off"/>
						<input type="text" style="display: none;" id="l2_stud_name" name="l2_stud_name" autocomplete="off"/>
						<button style="display: none;" class="btn btn-dark" title="Print Last Transaction" type="submit" id="btn_submit"><span class="fa fa-print"></span> Print
						</button>  
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Student Payment Details-->
<div class="modal fade" id="payment_details">
	<div class="modal-dialog modal-medium">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-money"></span> Payment Details: <strong id="p_name"></strong></p>
				<input type="hidden" id="p_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<div class="col-lg-12 input">
					<div class="input-group">
		                <input type="show" oninput="load_p_details();" class="form-control" oninput="" name="search_p" id="search_p" placeholder="Search Or#...">
		                <span class="input-group-addon"><i class="fa fa-search"></i></span>
		            </div>
				</div><br/>
				<div class="tale table-responsive">
					<table class="table table-sm table-hover table-striped">
						<thead>
							<tr>
								<th>Or#</th>
								<th>Payment</th>
								<th>balance</th>
								<th>Status</th>
								<th>Date</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="load_payment_details">
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer none" id="buttons">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Close</button>
				<form action="print_all_ledger.php" method="post">
					<input type="text" style="display: none" id="print_id" name="print_id" autocomplete="off"/>
					<button class="btn btn-dark" type="submit" id="btn_submit2"><span class="fa fa-print"></span> Print
					</button>  
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Delete Last Transaction-->
<div class="modal fade" id="delete_last_trans">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<p class="title"><span class="fa fa-money"></span> Delete Last Transction: <strong id="d_or_no"></strong></p>
				<input type="hidden" id="d_id">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<span id="cred">
					<div class="form-group">
					<label for="fn" class="w3-small">Enter Credential to delete Transaction:</label>
		            <input class="form-control" id="credential" name="credential" type="password" placeholder="Enter Credential">
		        </div>
		        <button class="btn btn-default"  onclick="verify_pass();"><span class="fa fa-check-circle"></span> Verify </button>
				</span>
				
		        <h4 class="hide del">Are you sure you want to delete this transaction?</h4>
			</div>
			<div class="modal-footer hide del">
				<button class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span> Cancel</button>
				<button class="btn btn-danger" disabled="" id="btn_del" onclick="delete_transaction();"><span class="fa fa-save"></span> Delete</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>