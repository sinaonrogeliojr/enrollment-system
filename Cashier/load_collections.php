<?php 
include('../db_config.php');
$search = mysqli_real_escape_string($con, $_POST['search_c']);
$date_ = date('Y-m-d');
if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT t4.`section`,t3.`grade_level`,t2.`stud_id` as studentid,DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') as dt,t1.*, CONCAT(t2.`ln`, ', ', t2.`mn`, ' ', t2.`ln`) as stud_name,t1.`type` FROM tbl_student_ledger t1 
		LEFT JOIN tbl_enrollment_form t2 ON t1.`stud_id` = t2.`id` 
		LEFT JOIN tbl_grade_level t3 ON t2.`grade_level` = t3.`id`
		LEFT JOIN tbl_section t4 ON t2.`section_id` = t4.`id` 
		WHERE DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') = '$date_' and t1.`Or_number` != '' ORDER BY t1.`date_trans` DESC");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr class="item">
				<td><?php echo $row['Or_number']; ?></td>
				<td><?php echo $row['studentid'];?></td>
				<td><?php echo $row['stud_name']; ?></td>
				<td><?php echo $row['grade_level']?></td>
				<td><?php echo $row['section']?></td>
				<td><?php echo $row['type']?></td>
				<td class="text-right">₱ <?php echo number_format($row['payment']); ?></td>
				<td><?php echo date('M-d-Y H:i A', strtotime($row['date_trans'])); ?></td>
			</tr>

		<?php }
	$date_ = date('Y-m-d');
	$total = mysqli_query($con, "SELECT sum(payment) as total FROM tbl_student_ledger WHERE DATE_FORMAT(date_trans,'%Y-%m%-%d') = '$date_' ");
	if(mysqli_num_rows($sql) > 0){
	$rows = mysqli_fetch_assoc($total);
	$total_collection = $rows['total']; ?>

	<tr class="bg-success text-white">
		<td colspan="5"></td>
		<td class="text-right">Total Collection:</td>
		<td class="text-right"><b>₱ <?php echo number_format($total_collection); ?></b></td>
		<td></td>
	</tr>

	<?php }else{

	}
	}else{ ?>
		<td colspan="7">No Collection for today!</td>;
   <?php }

}else{

	$sql = mysqli_query($con, "SELECT t4.`section`,t3.`grade_level`,t2.`stud_id` as studentid,DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') as dt,t1.*, CONCAT(t2.`ln`, ', ', t2.`mn`, ' ', t2.`ln`) as stud_name, t1.`type` FROM tbl_student_ledger t1 
		LEFT JOIN tbl_enrollment_form t2 ON t1.`stud_id` = t2.`id` 
		LEFT JOIN tbl_grade_level t3 ON t2.`grade_level` = t3.`id`
		LEFT JOIN tbl_section t4 ON t2.`section_id` = t4.`id` 
		WHERE DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') = '$date_' and t1.`Or_number` != '' and CONCAT(t2.`ln`, t2.`mn`, t2.`ln`,t1.`Or_number`,t4.`section`,t3.`grade_level`) like '%$search%'");

	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>

			<tr class="item">
				<td><?php echo $row['Or_number']; ?></td>
				<td><?php echo $row['studentid'];?></td>
				<td><?php echo $row['stud_name']; ?></td>
				<td><?php echo $row['grade_level']?></td>
				<td><?php echo $row['section']?></td>
				<td><?php echo $row['type']?></td>
				<td class="text-right">₱ <?php echo number_format($row['payment']); ?></td>
				<td><?php echo date('M-d-Y H:i A', strtotime($row['date_trans'])); ?></td>
			</tr>

		<?php }

		$date_ = date('Y-m-d');
	$total = mysqli_query($con, "SELECT sum(payment) as total FROM tbl_student_ledger WHERE DATE_FORMAT(date_trans,'%Y-%m%-%d') = '$date_' ");
	if(mysqli_num_rows($sql) > 0){
	$rows = mysqli_fetch_assoc($total);
	$total_collection = $rows['total']; ?>

	<tr class="bg-success text-white">
		<td colspan="5"></td>
		<td class="text-right">Total Collection:</td>
		<td class="text-right"><b>₱ <?php echo number_format($total_collection); ?></b></td>
		<td></td>
	</tr>

	<?php }else{

	}
	}else{ ?>
		<td colspan="7">No Collection for today!</td>;
   <?php }

}

?>