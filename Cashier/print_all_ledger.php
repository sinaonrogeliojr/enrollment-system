<?php 
include('../db_config.php');
$stud_id = $_POST['print_id'];
$sql = mysqli_query($con, "SELECT t1.*,t2.`section`,t2.`id` as sec_id,t3.`school_year`,t4.`grade_level`,t4.`id` as grade_id FROM tbl_enrollment_form t1 
			LEFT JOIN tbl_section t2 ON t1.`section_id` = t2.`id`
			LEFT JOIN tbl_grade_level t4 ON t1.`grade_level` = t4.`id`
			LEFT JOIN tbl_school_year t3 ON t1.`school_year` = t3.`id` WHERE t1.`stud_id` = '$stud_id'");
$row = mysqli_fetch_assoc($sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Student</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/print.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="col-lg-12 text-center">
		<img src="../img/logo.jpg" class="img-responsive" width="90px" height="90px">
		<h4>Galilee Integraded School</h4>
		<h5>Student Ledger</h5>
		<hr>
		<div class="col-lg-12 row">
			<div class="col-lg-1"></div>
			<div class="col-lg-7">
				<table>
					<tr>
						<td class="text-left">Student ID: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['stud_id']; ?></b></td>
					</tr>
					<tr>
						<td class="text-left">Student Name: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['ln'].', '. $row['fn'].' '. $row['mn']; ?></b></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-4 float-right">
				<table>
					<tr>
						<td class="text-right">Date Print: &nbsp;</td>
						<td class="text-left"><b><?php echo date('Y-m-d'); ?></b></td>
					</tr>
					<tr>
						<td class="text-right">School Year: &nbsp;</td>
						<td class="text-left"><b><?php echo $row['school_year']; ?></b></td>
					</tr>
				</table>
			</div>
		</div>
		<hr>
		<div class="col-lg-12">
			<table class="table table-sm">
				<thead>
					<tr>
						<th>Or#</th>
								<th>Payment</th>
								<th>balance</th>
								<th>Date</th>
								<th>Status</th>	
					</tr>
				</thead>
				<tbody>
					<?php 
						$sec_id = $row['sec_id'];
						$query = mysqli_query($con, "SELECT * FROM tbl_student_ledger WHERE stud_id = '$stud_id' ORDER BY id ASC");
						while ($rows = mysqli_fetch_assoc($query)) { ?>

							<tr>
							<td><?php echo $rows['Or_number']; ?></td>
							<td><?php echo $rows['payment']; ?></td>
							<td><?php echo $rows['balance']; ?></td>
							<td><?php echo date('Y-m-d',strtotime($rows['date_trans'])); ?></td>
							<td>
								<?php 
								$status='';

								if($rows['balance'] == 0.00){
									$status = '<label class="text-success"> Paid </label>';
								}else{
									$status = '<label class="text-danger"> Not Paid </label>';
								}
								echo $status;
								?>
								
							</td>
							
							<td>
							</tr>

						<?php
						}

					?>
				</tbody>
			</table>
		</div><br>
		<div class="col-lg-12 row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4" style="line-height: 15px;">
				<hr style="border: 1px solid #000;">
				Claudine Videz<br>
				Cashier
			</div>
		</div><br><br>
		<div class="col-lg-12" id="btns">
			<button class="btn btn-sm btn-default" onclick="print_preview();"><span class="fa fa-print"></span> Preview</button>
			<button class="btn btn-sm btn-default" onclick="window.location='billing.php'"> Back</button>
		</div>
		<br>
</div>
<script type="text/javascript">
	function print_preview(){
		$('#btns').addClass('none');
		window.print();
		$('#btns').removeClass('none');
	}
</script>
</body>
</html>