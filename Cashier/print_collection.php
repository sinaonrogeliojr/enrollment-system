<?php include('../db_config.php');?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Student</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">

  	<link rel="stylesheet" type="text/css" href="../bootstrap/css/print.css">
  	<link rel="stylesheet" type="text/css" href="../css/w3.css"/>
  	<link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.css"/>
  	<link rel="stylesheet" type="text/css" href="registrar.css"/>
  	<script type="text/javascript" src="../js/jquery.min.js"></script>
  	<script type="text/javascript" src="../js/popper.min.js"></script>
  	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
	<div class="col-lg-12 text-center">
		<img src="../img/logo.jpg" class="img-responsive" width="120px" height="120px">
		<h4>Galilee Integraded School</h4>
		<h3>Collection For Today</h3>
		<h6><?php echo date('M-d-Y'); ?></h6>
		<hr>
		<div class="col-lg-12 row">
			
			<div class="table table-responsive">
			<table class="table table-hover table-sm table-striped table-bordered" id="billing">
				<thead>
					<th>OR #</th>
					<th>Student ID</th>
					<th>Student</th>
					<th>Grade</th>
					<th>Section</th>
					<th>Type</th>
					<th>Payment</th>
					<th>Date</th>
				</thead>
				<tbody>
					
					<?php 

					$date_ = date('Y-m-d');
					$sql = mysqli_query($con, "SELECT t4.`section`,t3.`grade_level`,t2.`stud_id` as studentid,DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') as dt,t1.*, CONCAT(t2.`ln`, ', ', t2.`mn`, ' ', t2.`ln`) as stud_name,t1.`type` FROM tbl_student_ledger t1 
						LEFT JOIN tbl_enrollment_form t2 ON t1.`stud_id` = t2.`id`
						LEFT JOIN tbl_grade_level t3 ON t2.`grade_level` = t3.`id`
						LEFT JOIN tbl_section t4 ON t2.`section_id` = t4.`id`  
						WHERE DATE_FORMAT(t1.`date_trans`,'%Y-%m%-%d') = '$date_' and t1.`Or_number` != '' ORDER BY t1.`date_trans` DESC");
					if(mysqli_num_rows($sql)){

						while ($row = mysqli_fetch_assoc($sql)) { ?>

							<tr class="item">
								<td><?php echo $row['Or_number']; ?></td>
								<td class="text-left"><?php echo $row['studentid'];?></td>
								<td class="text-left"><?php echo $row['stud_name']; ?></td>
								<td><?php echo $row['grade_level']?></td>
								<td><?php echo $row['section']?></td>
								<td><?php echo $row['type']?></td>
								<td class="text-right">₱ <?php echo number_format($row['payment']); ?></td>
								<td><?php echo date('M-d-Y H:i A', strtotime($row['date_trans'])); ?></td>
							</tr>

						<?php }
					$date_ = date('Y-m-d');
					$total = mysqli_query($con, "SELECT sum(payment) as total FROM tbl_student_ledger WHERE DATE_FORMAT(date_trans,'%Y-%m%-%d') = '$date_' ");
					if(mysqli_num_rows($sql) > 0){
					$rows = mysqli_fetch_assoc($total);
					$total_collection = $rows['total']; ?>

					<tr class="bg-success text-white">
						<td colspan="4"></td>
						<td class="text-right">Total Collection:</td>
						<td class="text-right"><b>₱ <?php echo number_format($total_collection); ?></b></td>
						<td></td>
					</tr>

					<?php }else{

					}
					}else{ ?>
						<td colspan="6">No Collection for today!</td>;
				   <?php }
					?>
				</tbody>
			</table>
		</div>


		</div><br>
		<div class="col-lg-12 row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4"></div>
			<div class="col-lg-4" style="line-height: 15px;">
				<hr style="border: 1px solid #000;">
				Claudine Videz<br>
				Cashier
			</div>
		</div><br>
		<div class="col-lg-12" id="btns">
			<button class="btn btn-sm btn-default" onclick="print_preview();"><span class="fa fa-print"></span> Preview</button>
			<button class="btn btn-sm btn-default" onclick="window.location='collection.php'"> Back</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	function print_preview(){
		$('#btns').addClass('none');
		window.print();
		$('#btns').removeClass('none');
		//window.location = 'billing.php';
	}
</script>
</body>
</html>