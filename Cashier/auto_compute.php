<?php 
session_start();
include('../db_config.php');
$tuition = mysqli_real_escape_string($con, $_POST['tuition']);
$reg = mysqli_real_escape_string($con, $_POST['reg_fee']);
$misc = mysqli_real_escape_string($con, $_POST['misc_fee']);
$comp = mysqli_real_escape_string($con, $_POST['computer']);
$aircon = mysqli_real_escape_string($con, $_POST['aircon']);
$books = mysqli_real_escape_string($con, $_POST['books']);

$total = $tuition + $reg + $misc + $comp + $aircon + $books;

echo $total;

?>