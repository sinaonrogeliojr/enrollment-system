<?php 
include('../db_config.php');
$p_id = mysqli_real_escape_string($con, $_POST['p_id']);
$search = mysqli_real_escape_string($con, $_POST['search_p']);

$sql2 = mysqli_query($con, "SELECT id,stud_id,TYPE,payment,balance,date_trans,Or_number FROM tbl_student_ledger WHERE stud_id = '$p_id' AND id IN (SELECT MAX(id) FROM tbl_student_ledger WHERE stud_id = '$p_id' GROUP BY stud_id)");
$rows = mysqli_fetch_assoc($sql2);

echo $rows['id'];

if ($search == "" || $search == null) {

	$sql = mysqli_query($con, "SELECT * FROM tbl_student_ledger WHERE stud_id = '$p_id' ORDER BY id ASC");
	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { 
		$id = $row['id'];
		$id2 = $rows['id'];
			?>
			
			<tr class="item">
				<td><?php echo $row['Or_number']; ?></td>
				<td><?php echo $row['payment']; ?></td>
				<td><?php echo $row['balance']; ?></td>
				<td>
					<?php 
					$status='';

					if($row['balance'] == 0.00){
						$status = '<label class="text-success"> Paid </label>';
					}else{
						$status = '<label class="text-danger"> Not Paid </label>';
					}
					echo $status;
					?>
					
				</td>
				<td><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></td>
				<td>
					<?php 
					if($id == $id2 and $row['Or_number'] != null){ ?>
						<button class="btn btn-danger" onclick="get_last_trans('<?php echo $row['id'] ?>','<?php echo $row['Or_number'] ?>')"><span class="fa fa-trash"></span></button>
					<?php }else{ ?>
						<button disabled="" class="btn btn-danger"><span class="fa fa-trash"></span></button>		
					<?php }
					?>
				</td>
			</tr>

		<?php }

	}else{
		echo '<td colspan="9">No Student Found!</td>';
   }

}else{

	$sql = mysqli_query($con, "SELECT * FROM tbl_student_ledger WHERE stud_id = '$p_id' and concat(Or_number,payment,balance,date_trans) like '%$search%' ORDER BY id ASC");


	if(mysqli_num_rows($sql)){

		while ($row = mysqli_fetch_assoc($sql)) { ?>
			<tr class="item">
				<td><?php echo $row['Or_number']; ?></td>
				<td><?php echo $row['payment']; ?></td>
				<td><?php echo $row['balance']; ?></td>
				<td>
					<?php 
					$status='';

					if($row['balance'] == 0.00){
						$status = '<label class="text-success"> Paid </label>';
					}else{
						$status = '<label class="text-danger"> Not Paid </label>';
					}
					echo $status;
					?>
					
				</td>
				<td><?php echo date('Y-m-d',strtotime($row['date_trans'])); ?></td>
				<td>
				<button class="btn btn-danger"><span class="fa fa-trash"></span></button>					
				</td>
			</tr>
		<?php }

	}else{
		echo '<td colspan="9">No Student Found!</td>';
   }

}

?>